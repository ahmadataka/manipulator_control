#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/PoseArray.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64MultiArray.h"
#include "manipulator_control/Vector3Array.h"
#include "sensor_msgs/Joy.h"

using namespace std;
using namespace arma;

mat theta_d, theta;
geometry_msgs::PoseArray ee_pose_ar;
geometry_msgs::PoseArray cm_pose_ar;
std_msgs::Float32 speed_sent;
manipulator_control::Vector3Array ee_twist;
const int joint_num=3;
float dt, speed, speed_max;
float lee[joint_num], lcm[joint_num];
mat joy_mat;

void get_joy(const sensor_msgs::Joy::ConstPtr& msg)
{
  sensor_msgs::Joy joy_msg;
  float ds = 0.2;
  joy_msg = *msg;
  joy_mat << 0.0 << endr
	  << joy_msg.axes[0]*ds << endr
	  << joy_msg.axes[1]*ds << endr;

  for(unsigned char i=0; i<joint_num; i++)
    {
      theta(i,0) = theta(i,0)+joy_mat(i,0);
    }
}

void theta_input(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray variable;
  variable = *msg;

  //Callibration
  //   First joint
  // 2nd sensor data
  // 380 --> 80
  // 370 --> 50
  // 402 --> 110
  //

  //
  // Second joint
  // 3rd
  // 357, 332 --> 0
  // 402, 336 --> 30
  // 417, 336 --> 60
  //
  // 387 --> 0
  //410 --> 70
  // 4th
  // 362 --> -30
  // 392 --> -60

  theta(1,0) = ((variable.data[1]-380)*(-110.0+80.0)/(402-380)-80)*(M_PI/180);
  // if(variable.data[2]>=357)
  {
    // theta(2,0) = (variable.data[2]-357)*(60-0)/(417-357)*(M_PI/180);
  }
  // else
  // {
    theta(2,0) = (variable.data[3]-387)*(70-0)/(410-387)*(M_PI/180);
    if(theta(2,0)<0) theta(2,0) = 0;
  // }

  // for(unsigned char i=0; i<joint_num; i++)
  // {
  //   theta(i,0) = variable.data[i];
  // }
}

mat transform_mat(float angle, float length, unsigned char index)
{
  mat T;
  if((index==0) || (index==1))
  {
    T << cos(angle) << 0.0 << -sin(angle) << length*cos(angle) << endr
      << 0.0 << 1.0 << 0.0 << 0.0 << endr
      << sin(angle) << 0.0 << cos(angle) << length*sin(angle) << endr
      << 0.0 << 0.0 << 0.0 << 1.0 << endr;
  }
  else if(index == 2)
  {
    T << cos(angle) << -sin(angle) << 0.0 << length*cos(angle) << endr
      << sin(angle) << cos(angle) << 0.0 << length*sin(angle) << endr
      << 0.0 << 0.0 << 1.0 << 0.0 << endr
      << 0.0 << 0.0 << 0.0 << 1.0 << endr;
  }

  return T;

}

mat mat_to_list(mat joint_mat, unsigned char length)
{
  mat joint_ang;
  joint_ang.set_size(length,1);
  joint_ang.zeros();
  for(unsigned char i=0; i<length; i++)
  {
    joint_ang(i,0) = joint_mat(i,0);
  }

  return joint_ang;
}

mat len_to_list(unsigned char link, float length_final)
{
  mat len_list;
  len_list.set_size(link,1);

  for(unsigned char i=0; i<link; i++)
  {
    if(i!=(link-1)) len_list(i,0) = lee[i];
    else len_list(i,0) = length_final;
  }
  return len_list;
}

mat forward(mat angles, mat length_list)
{
  mat T_tot, transform;
  T_tot = eye<mat>(4,4);
  for(unsigned char i=0; i<angles.n_rows; i++)
  {
    transform = transform_mat(angles(i,0), length_list(i,0),i);
    T_tot = T_tot*transform;
  }

  return T_tot;
}

mat jacobian_update(unsigned char index)
{
  mat T_final, angles_new, Jacobi, T_final_new, angles;
  angles = mat_to_list(theta,(index));
  T_final = forward(angles, len_to_list((index), lee[index-1]));
  double delta = 0.00001;
  int number = angles.n_rows;
  angles_new.set_size(number, 1);
  angles_new.zeros();
  Jacobi.set_size(2,number);
  Jacobi.zeros();
  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles(i,0);
  }


  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles_new(i,0)+delta;
    T_final_new = forward(angles_new, len_to_list((index), lee[index-1]));
    Jacobi(0,i) = (T_final_new(0,3)-T_final(0,3))/delta;
    Jacobi(1,i) = (T_final_new(1,3)-T_final(1,3))/delta;
    angles_new(i,0) = angles_new(i,0)-delta;
  }

  return Jacobi;
}

mat RtoQuat(mat T_matrix)
{
  mat T_arma;

  double w, x, y, z;

  float trace = T_matrix(0,0) + T_matrix(1,1) + T_matrix(2,2); // I removed + 1.0f; see discussion with Ethan
  if( trace > 0 ) {// I changed M_EPSILON to 0
    float s = 0.5f / sqrtf(trace+ 1.0f);
    w = 0.25f / s;
    x = ( T_matrix(2,1) - T_matrix(1,2) ) * s;
    y = ( T_matrix(0,2) - T_matrix(2,0) ) * s;
    z = ( T_matrix(1,0) - T_matrix(0,1) ) * s;
  } else {
    if ( T_matrix(0,0) > T_matrix(1,1) && T_matrix(0,0) > T_matrix(2,2) ) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(0,0) - T_matrix(1,1) - T_matrix(2,2));
      w = (T_matrix(2,1) - T_matrix(1,2) ) / s;
      x = 0.25f * s;
      y = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      z = (T_matrix(0,2) + T_matrix(2,0) ) / s;
    } else if (T_matrix(1,1) > T_matrix(2,2)) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(1,1) - T_matrix(0,0) - T_matrix(2,2));
      w = (T_matrix(0,2) - T_matrix(2,0) ) / s;
      x = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      y = 0.25f * s;
      z = (T_matrix(1,2) + T_matrix(2,1) ) / s;
    } else {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(2,2) - T_matrix(0,0) - T_matrix(1,1) );
      w = (T_matrix(1,0) - T_matrix(0,1) ) / s;
      x = (T_matrix(0,2) + T_matrix(2,0) ) / s;
      y = (T_matrix(1,2) + T_matrix(2,1) ) / s;
      z = 0.25f * s;
    }
  }

  T_arma << x << endr
	 << y << endr
	 << z << endr
	 << w << endr;
  return T_arma;

}

void forward_out()
{
  mat T_out, quat, jacob, vel;
  geometry_msgs::Pose ee_pose;
  geometry_msgs::Pose cm_pose;
  geometry_msgs::Vector3 vel_vector;
  ee_pose_ar.poses.clear();
  cm_pose_ar.poses.clear();
  ee_twist.stamp = ros::Time::now();
  ee_twist.vector.clear();
  for(unsigned char i=0; i<theta.n_rows; i++)
  {
    T_out = forward(mat_to_list(theta,(i+1)), len_to_list((i+1), lee[i]));
    quat = RtoQuat(T_out);

    ee_pose.position.x = T_out(0,3);
    ee_pose.position.y = T_out(1,3);
    ee_pose.position.z = T_out(2,3);
    ee_pose.orientation.x = quat(0,0);
    ee_pose.orientation.y = quat(1,0);
    ee_pose.orientation.z = quat(2,0);
    ee_pose.orientation.w = quat(3,0);
    ee_pose_ar.poses.push_back(ee_pose);

    T_out = forward(mat_to_list(theta,(i+1)), len_to_list((i+1), lcm[i]));
    quat = RtoQuat(T_out);

    cm_pose.position.x = T_out(0,3);
    cm_pose.position.y = T_out(1,3);
    cm_pose.position.z = T_out(2,3);
    cm_pose.orientation.x = quat(0,0);
    cm_pose.orientation.y = quat(1,0);
    cm_pose.orientation.z = quat(2,0);
    cm_pose.orientation.w = quat(3,0);
    cm_pose_ar.poses.push_back(cm_pose);

    if(i==(joint_num-1))
    {
      jacob = jacobian_update(i+1);
      vel = jacob*theta_d;

      vel_vector.x = vel(0,0);
      vel_vector.y = vel(1,0);
      speed = pow((pow(vel_vector.x,2)+pow(vel_vector.y,2)),0.5);
      if(speed>speed_max)
      {
        speed_max = speed;
        speed_sent.data = speed_max;
      }
      ee_twist.vector.push_back(vel_vector);
    }
  }
  ee_pose_ar.header.stamp = ros::Time::now();
  cm_pose_ar.header.stamp = ros::Time::now();
}

int
main(int argc, char** argv)
{
  ros::init(argc,argv, "forward_kinematics_ncnr");
  ros::NodeHandle n;

  ros::Publisher pub_eepose = n.advertise<geometry_msgs::PoseArray>("/agent/ee/pose",10);
  ros::Publisher pub_cmpose = n.advertise<geometry_msgs::PoseArray>("/agent/cm/pose",10);
  ros::Publisher pub_speed = n.advertise<std_msgs::Float32>("/speed_max",10);
  ros::Publisher pub_eetwist = n.advertise<manipulator_control::Vector3Array>("/agent/ee/twist",10);
  ros::Subscriber sub_joint = n.subscribe("/sensor_read", 10, theta_input);
  ros::Subscriber goal_sub = n.subscribe("/joy", 10, get_joy);

  float lee_array[joint_num] = {0.5, 0.6, 0.5};

  for(unsigned char i=0; i<joint_num; i++)
  {
    lee[i]=lee_array[i];
    lcm[i]=(lee[i]/2.0);
  }

  float freq = 50.0;
  float dt = 1.0/freq;

  ros::Rate loop_rate(freq);
  theta.set_size(joint_num, 1);
  theta.zeros();
  theta(0,0) = 80.0*M_PI/180.0;
  theta_d.set_size(joint_num, 1);
  theta_d.zeros();
  while(ros::ok())
  {
    forward_out();
    (theta*180/M_PI).print("theta:");
    pub_eepose.publish(ee_pose_ar);
    pub_eetwist.publish(ee_twist);
    pub_cmpose.publish(cm_pose_ar);
    pub_speed.publish(speed_sent);
    ros::spinOnce();
    loop_rate.sleep();
  }


}
