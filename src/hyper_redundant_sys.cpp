#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float64.h"
#include "manipulator_control/JacobianField.h"

using namespace std;
using namespace arma;

mat force, theta_d_avoid, theta_d, theta, obstacle_field, sensor_jacobi, tip_field;
int joint_num, move_flag, obs_flag, sens_in, flag_jacobi;
float lee, dt, lcm;
sensor_msgs::JointState system_joint;
double max_norm, red_weight;
unsigned char flag, counting;
double max_dummy;
double find_magnitude(double input1, double input2)
{
  double magnitude = sqrt(pow(input1,2) + pow(input2,2));
  return magnitude;
}

void force_input(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 variable;
  variable = *msg;
  force << variable.x << endr
        << variable.y << endr;
}

void get_body(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 variable;
  variable = *msg;
  obstacle_field << variable.x << endr
                 << variable.y << endr;
}

void get_force_tip(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 variable;
  variable = *msg;
  tip_field << variable.x << endr
            << variable.y << endr;
}

void get_avoidance(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray variable;
  variable = *msg;
  for(unsigned char i=0; i<joint_num; i++)
  {
    theta_d_avoid(i,0) = variable.data[i];
  }
}

void jacobi_callback(const manipulator_control::JacobianField::ConstPtr& msg)
{
  manipulator_control::JacobianField jacobi_get;
  jacobi_get = *msg;

  sensor_jacobi.set_size(jacobi_get.jacobian.row, joint_num);
  unsigned char array_in = 0;
  for(unsigned char row_in = 0; row_in<jacobi_get.jacobian.row; row_in++)
  {
    for(unsigned char col_in = 0; col_in<joint_num; col_in++)
    {
      if(col_in < jacobi_get.jacobian.column)
      {
        sensor_jacobi(row_in,col_in) = jacobi_get.jacobian.matrix_data[array_in];
      }
      else
      {
        sensor_jacobi(row_in,col_in) = 0.0;
      }

      array_in++;
    }
  }

  flag_jacobi = 1;
}

void red_callback(const std_msgs::Float64::ConstPtr& msg)
{
  std_msgs::Float64 var;
  var = *msg;
  red_weight = var.data;
}

void get_sensin(const std_msgs::Int32::ConstPtr& msg)
{
  std_msgs::Int32 var;
  var = *msg;
  sens_in = var.data;
}

mat transform_mat(float angle)
{
  mat T;
  T << cos(angle) << -sin(angle) << 0.0 << lee*cos(angle) << endr
    << sin(angle) << cos(angle) << 0.0 << lee*sin(angle) << endr
    << 0.0 << 0.0 << 1.0 << 0.0 << endr
    << 0.0 << 0.0 << 0.0 << 1.0 << endr;
  return T;

}

mat forward(mat angles)
{
  mat T_tot, transform;
  T_tot = eye<mat>(4,4);

  for(unsigned char i=0; i<angles.n_rows; i++)
  {
    transform = transform_mat(angles(i,0));
    T_tot = T_tot*transform;
  }

  return T_tot;
}


mat jacobian_update(mat angles)
{
  mat T_final, angles_new, Jacobi, T_final_new;
  T_final = forward(angles);
  double delta = 0.00001;
  int number = angles.n_rows;
  angles_new.set_size(number, 1);
  angles_new.zeros();
  Jacobi.set_size(2,number);
  Jacobi.zeros();
  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles(i,0);
  }


  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles_new(i,0)+delta;
    T_final_new = forward(angles_new);
    Jacobi(0,i) = (T_final_new(0,3)-T_final(0,3))/delta;
    Jacobi(1,i) = (T_final_new(1,3)-T_final(1,3))/delta;
    angles_new(i,0) = angles_new(i,0)-delta;
  }

  return Jacobi;
}

mat forward_kinematics(mat angles)
{
  mat T_out, pose;
  T_out = forward(angles);
  pose << T_out(0,3) << endr
       << T_out(1,3) << endr;
  return pose;
}


void inv_kin(mat jacob)
{
  mat obstacle_term, tip_force;
  // force << 0.0 << endr
  //       << 0.0 << endr;

  if(move_flag == 1)
  {
    tip_force = force+tip_field;
    // tip_force = force;
    max_norm = 0.05;
    max_dummy = 0.1;
    if(norm(tip_force)>max_norm)
    {
      // cout << norm(tip_force) << endl;
      // max_norm = norm(tip_force);
      // cout << "norm" << endl;
      tip_force = (max_norm/norm(tip_force))*tip_force;
    }
    if(obs_flag==1)
    {
      if(flag_jacobi==1)
      {
        if(norm(obstacle_field)>max_dummy)
        {
          // max_dummy = norm(obstacle_field);
          // cout << "norm" << endl;
          obstacle_field = (max_dummy/norm(obstacle_field))*obstacle_field;
        }
        // obstacle_term = red_weight*pinv(sensor_jacobi*(eye<mat>(joint_num,joint_num)-pinv(jacob)*jacob))*(obstacle_field - sensor_jacobi*(pinv(jacob)*force+theta_d_avoid));
        // obstacle_term = red_weight*pinv(sensor_jacobi*(eye<mat>(joint_num,joint_num)-pinv(jacob)*jacob))*(obstacle_field - sensor_jacobi*(pinv(jacob)*(tip_force)));
        obstacle_term = pinv(sensor_jacobi)*obstacle_field;
        // obstacle_term.print("obs_term:");
        // theta_d = pinv(jacob)*force+  obstacle_term + theta_d_avoid;
        theta_d = pinv(jacob)*tip_force+  obstacle_term;
        // force.print("goal:");
        // theta_d.print("theta_d");
        // obstacle_field.print("obs:");
        // sensor_jacobi.print("jacobi:");
      }

    }
    else
    {
      // theta_d = pinv(jacob)*force + theta_d_avoid;
      theta_d = pinv(jacob)*tip_force;
    }

    // cout << "move" << endl;
  }
  else
  {
    // cout << "stop" << endl;
    theta_d.set_size(joint_num,1);
    theta_d.zeros();
  }

  // theta_d.set_size(joint_num,1);
  // theta_d.ones();
  // theta_d = theta_d*0.01;
  // jacob.print("Jac:");

  // int max_norm = 5.0;
  // cout << norm(theta_d) << endl;
  // if(norm(theta_d)>max_norm)
  // {
  //   cout << "norm" << endl;
  //   theta_d = (max_norm/norm(theta_d))*theta_d;
  // }

  theta = theta + theta_d*dt;
}

void transform_to_sent()
{
  system_joint.header.stamp = ros::Time::now();
  system_joint.name.clear();
  system_joint.position.clear();
  system_joint.velocity.clear();
  for(unsigned char i=0; i<joint_num; i++)
  {
    system_joint.name.clear();
    system_joint.position.push_back(theta(i,0));
    system_joint.velocity.push_back(theta_d(i,0));
  }

}

int
main(int argc, char** argv)
{
    ros::init(argc,argv, "hyper_redundant_sys");
    ros::NodeHandle n;
    n.param("joint_num", joint_num, joint_num);
    ros::Time begin, current;
    ros::Publisher pub_pose = n.advertise<sensor_msgs::JointState>("/agent/joint",10);
    ros::Subscriber sub_force = n.subscribe("force", 10, force_input);
    ros::Subscriber sub_field = n.subscribe("/agent/collision_avoidance", 10, get_avoidance);
    ros::Subscriber sub_redund = n.subscribe("/redundancy_weight", 10, red_callback);
    ros::Subscriber sub_body = n.subscribe("/force_body_obs", 10, get_body);
    ros::Subscriber sub_tip = n.subscribe("/force_obs", 10, get_force_tip);
    ros::Subscriber sub_sens = n.subscribe("/agent/sensor_index", 10, get_sensin);
    ros::Subscriber jacobi_sub = n.subscribe("jacobian_body", 10, jacobi_callback);

    // lcm = 0.25;
    // lee = 0.5;
    lee = 0.02;
    lcm = lee/2.0;

    theta.set_size(joint_num, 1);
    theta.zeros();
    for(unsigned char i=0; i < joint_num; i++)
    {
      if(i==0) theta(i,0) = M_PI/2.0;
      else if(i>0 && i<(joint_num/2)) theta(i,0) = - M_PI/6.0;
      else theta(i,0) = M_PI/6.0;
      // theta(i,0) = M_PI/3;
    }

    force.set_size(2,1);
    force.zeros();
    theta_d_avoid.set_size(joint_num,1);
    theta_d_avoid.zeros();
    float freq = 50.0;
    dt = 1.0/freq;
    ros::Rate loop_rate(freq);
    flag = 0;
    counting = 0;
    max_norm = 0.0;
    max_dummy = 0.0;
    begin = ros::Time::now();
    while(ros::ok())
    {
      n.param("starting", move_flag, move_flag);
      n.param("obstacle_start", obs_flag, obs_flag);
      // cout << move_flag << endl;
      current = ros::Time::now();
      if((current.sec-begin.sec) > 2)
      {
        inv_kin(jacobian_update(theta));
        transform_to_sent();
        pub_pose.publish(system_joint);
      }

      ros::spinOnce();
      loop_rate.sleep();
    }
}
