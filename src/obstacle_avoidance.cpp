#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Vector3.h"
#include "manipulator_control/Vector3Array.h"
#include "manipulator_control/JacobianField.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Twist.h"

using namespace std;
using namespace arma;

mat theta_d, theta, current_pose, force, tip_pose;
geometry_msgs::Vector3 dist_to_obs, vel_obs, sens_to_obs, vel_body, vel_tip;
geometry_msgs::Twist sp;
std_msgs::Float64MultiArray theta_d_obs;
int field_type, joint_num, sens_in;
float lee, dt, lcm;
std_msgs::Float32 distance_sca;
double redundancy_weight;
manipulator_control::JacobianField jacobi_sent;
std_msgs::Int32 magnet_flag;
void theta_input(const sensor_msgs::JointState::ConstPtr& msg)
{
  sensor_msgs::JointState variable;
  variable = *msg;

  for(unsigned char i=0; i<joint_num; i++)
  {
    theta(i,0) = variable.position[i];
    theta_d(i,0) = variable.velocity[i];
  }
}

void get_distobs(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  dist_to_obs.x = var.x;
  dist_to_obs.y = var.y;
}

void get_sensobs(const geometry_msgs::Vector3::ConstPtr& msg)
{
  geometry_msgs::Vector3 var;
  var = *msg;
  sens_to_obs.x = var.x;
  sens_to_obs.y = var.y;
}

void get_sensin(const std_msgs::Int32::ConstPtr& msg)
{
  std_msgs::Int32 var;
  var = *msg;
  sens_in = var.data;
}

void get_twist(const manipulator_control::Vector3Array::ConstPtr& msg)
{
  manipulator_control::Vector3Array var;
  var = *msg;
  sp.linear.x = var.vector[0].x;
  sp.linear.y = var.vector[0].y;
}

void get_tip(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  geometry_msgs::PoseArray var;
  var = *msg;
  tip_pose << var.poses[joint_num-1].position.x << endr
           << var.poses[joint_num-1].position.y << endr
           << var.poses[joint_num-1].position.z << endr;
}

mat transform_mat(float angle, float length)
{
  mat T;
  T << cos(angle) << -sin(angle) << 0.0 << length*cos(angle) << endr
    << sin(angle) << cos(angle) << 0.0 << length*sin(angle) << endr
    << 0.0 << 0.0 << 1.0 << 0.0 << endr
    << 0.0 << 0.0 << 0.0 << 1.0 << endr;
  return T;
}

mat mat_to_list(mat joint_mat, unsigned char length)
{
  mat joint_ang;
  joint_ang.set_size(length,1);
  joint_ang.zeros();
  for(unsigned char i=0; i<length; i++)
  {
    joint_ang(i,0) = joint_mat(i,0);
  }

  return joint_ang;
}

mat len_to_list(unsigned char link, float length_final)
{
  mat len_list;
  len_list.set_size(link,1);

  for(unsigned char i=0; i<link; i++)
  {
    if(i!=(link-1)) len_list(i,0) = lee;
    else len_list(i,0) = length_final;
  }
  return len_list;
}

mat forward(mat angles, mat length_list)
{
  mat T_tot, transform;
  T_tot = eye<mat>(4,4);
  for(unsigned char i=0; i<angles.n_rows; i++)
  {
    transform = transform_mat(angles(i,0), length_list(i,0));
    T_tot = T_tot*transform;
  }

  return T_tot;
}

mat jacobian_update(unsigned char index)
{
  mat T_final, angles_new, Jacobi, T_final_new, angles;
  angles = mat_to_list(theta,(index));
  T_final = forward(angles, len_to_list((index), lee));
  double delta = 0.00001;
  int number = angles.n_rows;
  angles_new.set_size(number, 1);
  angles_new.zeros();
  Jacobi.set_size(2,number);
  Jacobi.zeros();
  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles(i,0);
  }


  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles_new(i,0)+delta;
    T_final_new = forward(angles_new, len_to_list((index), lee));
    Jacobi(0,i) = (T_final_new(0,3)-T_final(0,3))/delta;
    Jacobi(1,i) = (T_final_new(1,3)-T_final(1,3))/delta;
    angles_new(i,0) = angles_new(i,0)-delta;
  }

  return Jacobi;
}

double find_magnitude(double input1, double input2)
{
  double magnitude = sqrt(pow(input1,2) + pow(input2,2));
  return magnitude;
}

void send_message(mat vect)
{
  theta_d_obs.data.clear();
  for(unsigned char i=0; i<joint_num; i++)
  {
    theta_d_obs.data.push_back(vect(i,0));
  }
}

void send_jacobi(mat jac)
{
  jacobi_sent.jacobian.row = jac.n_rows;
  jacobi_sent.jacobian.column = jac.n_cols;
  jacobi_sent.jacobian.matrix_data.clear();
  for(unsigned char i=0; i<jac.n_rows; i++)
  {
    for(unsigned char j=0; j<jac.n_cols; j++)
    {
        jacobi_sent.jacobian.matrix_data.push_back(jac(i,j));
    }
  }
}

void collision_avoid_tip()
{
  double dist_const = 0.0;
  double bound = 0.03;
  double constant = 0.00001;
  double constant2 = 3.0;
  double distance, speed, mag;
  mat agent_cur, dist_from_obs, obs_cur, lc_cross, B, Force, la_cross;
  distance = find_magnitude(dist_to_obs.x, dist_to_obs.y) - dist_const;
  speed = find_magnitude(sp.linear.x, sp.linear.y);
  if(field_type == 0)
  {
    if(distance > bound)
    {
      vel_obs.x = 0;
      vel_obs.y = 0;
    }

    else
    {
      if(distance !=0)
      {
        vel_obs.y = -constant*(1/distance - 1/bound)*(dist_to_obs.y)/(pow(distance,3));
        vel_obs.x = -constant*(1/distance - 1/bound)*(dist_to_obs.x)/(pow(distance,3));
      }
    }
  }

  else if(field_type == 1)
  {
    if((dist_to_obs.x!=0) && (dist_to_obs.y!=0))
    {
      if(distance > (bound))
      {
        vel_obs.x = 0;
        vel_obs.y = 0;
        magnet_flag.data = 0;
      }
      else
      {
        magnet_flag.data = 1;
        // cout << "obs" << endl;
        agent_cur << sp.linear.x << endr
                  << sp.linear.y << endr
                  << 0.0 << endr;
        if(speed!=0) agent_cur = agent_cur/(speed);
        dist_from_obs << -1*dist_to_obs.x << endr
                      << -1*dist_to_obs.y << endr
                      << -1*dist_to_obs.z << endr;
        obs_cur = agent_cur - (dot(agent_cur, dist_from_obs)/norm(dist_from_obs))*(dist_from_obs/norm(dist_from_obs));
        mag = pow((pow(obs_cur(0,0),2)+pow(obs_cur(1,0),2)+pow(obs_cur(2,0),2)),0.5);

        if(mag <= 0.05)
        {
          // cout << "90" << endl;
          obs_cur << 0 << 1 << 0 << endr
                  << -1 << 0 << 0 << endr
                  << 0 << 0 << 1 << endr;
          obs_cur = obs_cur*agent_cur;
          // obs_cur = obs_cur*dist_from_obs*(-1);
          // cout << obs_cur.t()*tip_pose << endl;
          mat obs_base = obs_cur.t()*tip_pose;
          float obs_base_ = obs_base(0,0);
          // cout << obs_base_ << endl;
          if(obs_base_>0)
          {
            cout << "change" << endl;
            obs_cur << 0 << -1 << 0 << endr
                    << 1 << 0 << 0 << endr
                    << 0 << 0 << 1 << endr;
            obs_cur = obs_cur*agent_cur;
          }
        }
        lc_cross << 0 << -obs_cur(2,0) << obs_cur(1,0) << endr
                 << obs_cur(2,0) << 0 << -obs_cur(0,0) << endr
                 << -obs_cur(1,0) << obs_cur(0,0) << 0 << endr;
        B << sp.linear.x << endr
          << sp.linear.y << endr
          << 0 << endr;
        B = lc_cross*B/distance;
        la_cross << 0 << -agent_cur(2,0) << agent_cur(1,0) << endr
                 << agent_cur(2,0) << 0 << -agent_cur(0,0) << endr
                 << -agent_cur(1,0) << agent_cur(0,0) << 0 << endr;
        Force = constant2*la_cross*B;
        vel_obs.x = sp.linear.x + Force(0,0)*dt;
        vel_obs.y = sp.linear.y + Force(1,0)*dt;
      }
    }
  }

  force << vel_obs.x << endr
        << vel_obs.y << endr;

  distance_sca.data = pow((pow(dist_to_obs.x,2)+pow(dist_to_obs.y,2)),0.5);
}

void collision_avoid_body()
{
  double bound = 0.03;
  double distance;
  double constant = 0.00000002;
  double dist_const = 0.0;
  distance = find_magnitude(sens_to_obs.x, sens_to_obs.y) - dist_const;
  // cout << distance << endl;
  if(distance > bound)
  {
    vel_body.x = 0;
    vel_body.y = 0;
  }

  else
  {
    // cout << "body" << endl;
    if(distance !=0)
    {
      vel_body.y = -constant*(1/distance - 1/bound)*(sens_to_obs.y)/(pow(distance,3));
      vel_body.x = -constant*(1/distance - 1/bound)*(sens_to_obs.x)/(pow(distance,3));
      // vel_body.y = -2*(1 - pow(sin(M_PI*distance/(2*bound)),0.25))*(sens_to_obs.y)/(distance);
      // vel_body.x = -2*(1 - pow(sin(M_PI*distance/(2*bound)),0.25))*(sens_to_obs.x)/(distance);
    }
  }
  if(distance>=1.1*bound)
  {
    redundancy_weight = 0;
    // cout << "2" << endl;
  }
  else if(distance>bound && distance<1.1*bound)
  {
    redundancy_weight = pow(cos((M_PI/2)*((distance-bound)/(0.1*bound))),2);
    // cout << "1" << endl;
  }
  else if(distance < bound)
  {
    redundancy_weight = 1;
    // cout << "0" << endl;
  }
}

int
main(int argc, char** argv)
{
  ros::init(argc,argv, "obstacle_avoidance");
  ros::NodeHandle n;
  n.param("joint_num", joint_num, joint_num);
  n.param("field_type", field_type, field_type);
  ros::Publisher pub_eetwist = n.advertise<std_msgs::Float64MultiArray>("/agent/collision_avoidance",10);
  ros::Publisher distance_pub = n.advertise<std_msgs::Float32>("/agent/dist_to_obs_scalar",10);
  ros::Publisher pub_force = n.advertise<geometry_msgs::Vector3>("/force_obs",10);
  ros::Publisher redundancy_pub = n.advertise<std_msgs::Float64>("redundancy_weight",10);
  ros::Publisher forcebody_pub = n.advertise<geometry_msgs::Vector3>("force_body_obs",10);
  ros::Publisher jacobi_pub = n.advertise<manipulator_control::JacobianField>("jacobian_body",10);
  ros::Publisher magnetic_pub = n.advertise<std_msgs::Int32>("/magnetic_flag",10);

  ros::Subscriber sub_distobs = n.subscribe("/agent/dist_to_obs", 10, get_distobs);
  ros::Subscriber sub_distsens = n.subscribe("/agent/sensor_to_obs", 10, get_sensobs);
  ros::Subscriber sub_sens = n.subscribe("/agent/sensor_index", 10, get_sensin);
  ros::Subscriber sub_joint = n.subscribe("/agent/joint", 10, theta_input);
  ros::Subscriber sub_twist = n.subscribe("/agent/ee/twist", 10, get_twist);
  ros::Subscriber sub_tip = n.subscribe("/agent/ee/pose", 10, get_tip);
  std_msgs::Float64 redundancy_sent;
  mat jacobi, theta_dot, jacobi_body;
  lee = 0.02;
  lcm = lee/2.0;
  theta.set_size(joint_num, 1);
  theta.zeros();
  theta_d.set_size(joint_num, 1);
  theta_d.zeros();
  float freq = 50.0;
  dt = 1.0/freq;
  ros::Rate loop_rate(freq);
  force.set_size(2, 1);
  force.zeros();
  while(ros::ok())
  {
    collision_avoid_tip();
    collision_avoid_body();
    jacobi = jacobian_update(joint_num);
    jacobi_body = jacobian_update(sens_in+1);
    theta_dot = pinv(jacobi)*force;
    send_message(theta_dot);
    send_jacobi(jacobi_body);
    redundancy_sent.data = redundancy_weight;

    pub_force.publish(vel_obs);
    pub_eetwist.publish(theta_d_obs);
    distance_pub.publish(distance_sca);
    redundancy_pub.publish(redundancy_sent);
    forcebody_pub.publish(vel_body);
    jacobi_pub.publish(jacobi_sent);
    magnetic_pub.publish(magnet_flag);
    ros::spinOnce();
    loop_rate.sleep();
  }

}
