#include "ros/ros.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/PoseArray.h"
#include "std_msgs/Float32.h"
#include "manipulator_control/Vector3Array.h"

using namespace std;
using namespace arma;

mat theta_d, theta;
geometry_msgs::PoseArray ee_pose_ar;
geometry_msgs::PoseArray cm_pose_ar;
std_msgs::Float32 speed_sent;
manipulator_control::Vector3Array ee_twist;
int joint_num;
float lee, dt, lcm, speed, speed_max;

void theta_input(const sensor_msgs::JointState::ConstPtr& msg)
{
  sensor_msgs::JointState variable;
  variable = *msg;

  for(unsigned char i=0; i<joint_num; i++)
  {
    theta(i,0) = variable.position[i];
    theta_d(i,0) = variable.velocity[i];
  }
}

mat transform_mat(float angle, float length)
{
  mat T;
  T << cos(angle) << -sin(angle) << 0.0 << length*cos(angle) << endr
    << sin(angle) << cos(angle) << 0.0 << length*sin(angle) << endr
    << 0.0 << 0.0 << 1.0 << 0.0 << endr
    << 0.0 << 0.0 << 0.0 << 1.0 << endr;
  return T;

}

mat mat_to_list(mat joint_mat, unsigned char length)
{
  mat joint_ang;
  joint_ang.set_size(length,1);
  joint_ang.zeros();
  for(unsigned char i=0; i<length; i++)
  {
    joint_ang(i,0) = joint_mat(i,0);
  }

  return joint_ang;
}

mat len_to_list(unsigned char link, float length_final)
{
  mat len_list;
  len_list.set_size(link,1);

  for(unsigned char i=0; i<link; i++)
  {
    if(i!=(link-1)) len_list(i,0) = lee;
    else len_list(i,0) = length_final;
  }
  return len_list;
}

mat forward(mat angles, mat length_list)
{
  mat T_tot, transform;
  T_tot = eye<mat>(4,4);
  for(unsigned char i=0; i<angles.n_rows; i++)
  {
    transform = transform_mat(angles(i,0), length_list(i,0));
    T_tot = T_tot*transform;
  }

  return T_tot;
}

mat jacobian_update(unsigned char index)
{
  mat T_final, angles_new, Jacobi, T_final_new, angles;
  angles = mat_to_list(theta,(index));
  T_final = forward(angles, len_to_list((index), lee));
  double delta = 0.00001;
  int number = angles.n_rows;
  angles_new.set_size(number, 1);
  angles_new.zeros();
  Jacobi.set_size(2,number);
  Jacobi.zeros();
  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles(i,0);
  }


  for(unsigned char i=0; i<number; i++)
  {
    angles_new(i,0) = angles_new(i,0)+delta;
    T_final_new = forward(angles_new, len_to_list((index), lee));
    Jacobi(0,i) = (T_final_new(0,3)-T_final(0,3))/delta;
    Jacobi(1,i) = (T_final_new(1,3)-T_final(1,3))/delta;
    angles_new(i,0) = angles_new(i,0)-delta;
  }

  return Jacobi;
}

mat RtoQuat(mat T_matrix)
{
  mat T_arma;

  double w, x, y, z;

  float trace = T_matrix(0,0) + T_matrix(1,1) + T_matrix(2,2); // I removed + 1.0f; see discussion with Ethan
  if( trace > 0 ) {// I changed M_EPSILON to 0
    float s = 0.5f / sqrtf(trace+ 1.0f);
    w = 0.25f / s;
    x = ( T_matrix(2,1) - T_matrix(1,2) ) * s;
    y = ( T_matrix(0,2) - T_matrix(2,0) ) * s;
    z = ( T_matrix(1,0) - T_matrix(0,1) ) * s;
  } else {
    if ( T_matrix(0,0) > T_matrix(1,1) && T_matrix(0,0) > T_matrix(2,2) ) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(0,0) - T_matrix(1,1) - T_matrix(2,2));
      w = (T_matrix(2,1) - T_matrix(1,2) ) / s;
      x = 0.25f * s;
      y = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      z = (T_matrix(0,2) + T_matrix(2,0) ) / s;
    } else if (T_matrix(1,1) > T_matrix(2,2)) {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(1,1) - T_matrix(0,0) - T_matrix(2,2));
      w = (T_matrix(0,2) - T_matrix(2,0) ) / s;
      x = (T_matrix(0,1) + T_matrix(1,0) ) / s;
      y = 0.25f * s;
      z = (T_matrix(1,2) + T_matrix(2,1) ) / s;
    } else {
      float s = 2.0f * sqrtf( 1.0f + T_matrix(2,2) - T_matrix(0,0) - T_matrix(1,1) );
      w = (T_matrix(1,0) - T_matrix(0,1) ) / s;
      x = (T_matrix(0,2) + T_matrix(2,0) ) / s;
      y = (T_matrix(1,2) + T_matrix(2,1) ) / s;
      z = 0.25f * s;
    }
  }

  T_arma << x << endr
	 << y << endr
	 << z << endr
	 << w << endr;
  return T_arma;

}

void forward_out()
{
  mat T_out, quat, jacob, vel;
  geometry_msgs::Pose ee_pose;
  geometry_msgs::Pose cm_pose;
  geometry_msgs::Vector3 vel_vector;
  ee_pose_ar.poses.clear();
  cm_pose_ar.poses.clear();
  ee_twist.stamp = ros::Time::now();
  ee_twist.vector.clear();
  for(unsigned char i=0; i<theta.n_rows; i++)
  {
    T_out = forward(mat_to_list(theta,(i+1)), len_to_list((i+1), lee));
    quat = RtoQuat(T_out);

    ee_pose.position.x = T_out(0,3);
    ee_pose.position.y = T_out(1,3);
    ee_pose.orientation.x = quat(0,0);
    ee_pose.orientation.y = quat(1,0);
    ee_pose.orientation.z = quat(2,0);
    ee_pose.orientation.w = quat(3,0);
    ee_pose_ar.poses.push_back(ee_pose);

    T_out = forward(mat_to_list(theta,(i+1)), len_to_list((i+1), lcm));
    quat = RtoQuat(T_out);

    cm_pose.position.x = T_out(0,3);
    cm_pose.position.y = T_out(1,3);
    cm_pose.orientation.x = quat(0,0);
    cm_pose.orientation.y = quat(1,0);
    cm_pose.orientation.z = quat(2,0);
    cm_pose.orientation.w = quat(3,0);
    cm_pose_ar.poses.push_back(cm_pose);

    if(i==(joint_num-1))
    {
      jacob = jacobian_update(i+1);
      vel = jacob*theta_d;

      vel_vector.x = vel(0,0);
      vel_vector.y = vel(1,0);
      speed = pow((pow(vel_vector.x,2)+pow(vel_vector.y,2)),0.5);
      if(speed>speed_max)
      {
        speed_max = speed;
        speed_sent.data = speed_max;
      }
      ee_twist.vector.push_back(vel_vector);
    }
  }
  ee_pose_ar.header.stamp = ros::Time::now();
  cm_pose_ar.header.stamp = ros::Time::now();
}

int
main(int argc, char** argv)
{
  ros::init(argc,argv, "forward_kinematics");
  ros::NodeHandle n;
  n.param("joint_num", joint_num, joint_num);
  ros::Publisher pub_eepose = n.advertise<geometry_msgs::PoseArray>("/agent/ee/pose",10);
  ros::Publisher pub_cmpose = n.advertise<geometry_msgs::PoseArray>("/agent/cm/pose",10);
  ros::Publisher pub_speed = n.advertise<std_msgs::Float32>("/speed_max",10);
  ros::Publisher pub_eetwist = n.advertise<manipulator_control::Vector3Array>("/agent/ee/twist",10);
  ros::Subscriber sub_joint = n.subscribe("/agent/joint", 10, theta_input);

  lee = 0.02;
  lcm = lee/2.0;

  float freq = 50.0;
  float dt = 1.0/freq;

  ros::Rate loop_rate(freq);
  theta.set_size(joint_num, 1);
  theta.zeros();
  theta_d.set_size(joint_num, 1);
  theta_d.zeros();
  while(ros::ok())
  {
    forward_out();

    pub_eepose.publish(ee_pose_ar);
    pub_eetwist.publish(ee_twist);
    pub_cmpose.publish(cm_pose_ar);
    pub_speed.publish(speed_sent);
    ros::spinOnce();
    loop_rate.sleep();
  }


}
