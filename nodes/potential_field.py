#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
import tf

class external_force(object):
  def __init__(self):
    rospy.init_node('external_force')
    self.force = Vector3()
    self.stiffness = Vector3()
    self.goal_sent = Pose()
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    force_pub = rospy.Publisher('force', Vector3, queue_size = 10)
    stiffness_pub = rospy.Publisher('stiffness', Vector3, queue_size = 10)
    goal_pub = rospy.Publisher('goal_pose', Pose, queue_size = 10)
    sub_eepose = rospy.Subscriber('/agent/ee/pose', PoseArray, self.get_pose)
    self.force.x = 0.0
    self.force.y = 0.0
    self.stiffness.x = 0.5
    self.stiffness.y = 0.5
    r = rospy.Rate(50.0)
    self.goal = [0.13, 0.07]
    self.current_pose = [0.13, 0.07]
    self.delta_f = 0.01

    while not rospy.is_shutdown():
        br = tf.TransformBroadcaster()
        br.sendTransform((self.goal[0], self.goal[1], 0.0),
                         (0.0, 0.0, 0.0, 1.0),
                         rospy.Time.now(),
                         "target",
                         "world")

        self.force.x = -self.stiffness.x*(self.current_pose[0]-self.goal[0])
        self.force.y = -self.stiffness.y*(self.current_pose[1]-self.goal[1])
        # print self.force
        force_pub.publish(self.force)
        stiffness_pub.publish(self.stiffness)
        goal_pub.publish(self.goal_sent)
        r.sleep()

        #rospy.spin()

  def ps3_callback(self,msg):
    self.goal[0] += msg.axes[0]*self.delta_f
    self.goal[1] += msg.axes[1]*self.delta_f
    self.goal_sent.position.x = self.goal[0]
    self.goal_sent.position.y = self.goal[1]
    if(msg.axes[2]==1):
        rospy.set_param("/starting", 1)
    elif(msg.axes[2]==-1):
        rospy.set_param("/starting", 0)
    if((msg.buttons[3]-msg.buttons[1])==1):
        rospy.set_param("/obstacle_start", 1)
    elif((msg.buttons[3]-msg.buttons[1])==-1):
        rospy.set_param("/obstacle_start", 0)


  def get_pose(self, msg):
    joint_ind = len(msg.poses)-1
    self.current_pose[0] = msg.poses[joint_ind].position.x
    self.current_pose[1] = msg.poses[joint_ind].position.y

if __name__ == '__main__':
    try:
        external_force()
    except rospy.ROSInterruptException: pass
