#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Twist

def find_magnitude(input1, input2):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
    return magnitude

def dot_product(A, B):
    return A[0,0]*B[0,0]+A[1,0]*B[1,0]+A[2,0]*B[2,0]

def angle(A,B):
    return dot_product(A,B)/(find_magnitude(A[0,0],A[1,0])*find_magnitude(B[0,0],B[1,0]))

if __name__ == '__main__':
    rospy.init_node('obstacle_distance')
    obstacle_number = rospy.get_param('/ObstacleNumber')
    obs_name = rospy.get_param('/ObstacleName')
    joint_num = rospy.get_param('/joint_num')
    listener = tf.TransformListener()

    turtle_vel = rospy.Publisher('/agent/dist_to_obs', Vector3, queue_size = 10)
    # turtle_sca = rospy.Publisher('/agent/dist_to_obs_scalar', Float32, queue_size = 10)
    cur_obs = rospy.Publisher('current_obs_global', Vector3, queue_size = 10)

    flag_listener = 0
    dist_obs = Vector3()
    cur_obs_dir = Vector3()
    dist_obs_sca = Float32()
    obstacle_index = String()

    rate = rospy.Rate(50.0)

    while not rospy.is_shutdown():
      obstacle_pose = []

      # Obstacle Individual Position Vector
      for i in range(0, (obstacle_number)):
	obstacle_index = ''
	obstacle_index = str(i+1)
	try:
            (trans,rot) = listener.lookupTransform('world/ee'+str(joint_num-1), obs_name+obstacle_index, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	obstacle_pose.append(Matrix([[trans[0]], [trans[1]], [trans[2]]]))
	flag_listener = 1
      minim = 100
      #print(flag_listener)
      # Obstacle's closest distance
      if(flag_listener == 1):
	for i in range(0, (obstacle_number)):
	  a_vec = obstacle_pose[i]
	  if(i==(obstacle_number-1)):
	    b_vec = obstacle_pose[0]
	  else:
	    b_vec = obstacle_pose[i+1]
	  c_vec = b_vec - a_vec
	  theta1 = angle(-a_vec, c_vec)
	  theta2 = angle(b_vec, c_vec)

	  if(theta1<0.0):
	    d_vec = a_vec
	    #print("Case1")
	  elif(theta2<0.0):
	    d_vec = b_vec
	    #print("Case2")
	  else:
	    d_vec = a_vec - dot_product(a_vec, c_vec)/pow(find_magnitude(c_vec[0,0], c_vec[1,0]),2)*c_vec
	    #print("Case3")
	  if(find_magnitude(d_vec[0,0],d_vec[1,0])<minim):
	    distance = d_vec
	    minim = find_magnitude(d_vec[0,0],d_vec[1,0])
	    cur_obs_vec = c_vec/find_magnitude(c_vec[0,0], c_vec[1,0])
	  if(obstacle_number == 2):
	    break
      #print(active_obstacle)
	dist_obs.x = distance[0,0]
	dist_obs.y = distance[1,0]
	dist_obs.z = distance[2,0]
	cur_obs_dir.x = cur_obs_vec[0,0]
	cur_obs_dir.y = cur_obs_vec[1,0]
	cur_obs_dir.z = cur_obs_vec[2,0]
      turtle_vel.publish(dist_obs)
    #   turtle_sca.publish(dist_obs_sca)
      rate.sleep()
