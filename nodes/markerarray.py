#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseArray

class markers(object):
  def __init__(self):
    rospy.init_node('markers')
    self.frame_number = rospy.get_param('~links')
    self.cm_pose = PoseArray()
    lcm = 0.25
    self.lee = 0.02

    self.marker_pub = rospy.Publisher('markers', MarkerArray, queue_size = 10)
    config_space_sub = rospy.Subscriber('/agent/cm/pose', PoseArray, self.get_pose)

    r = rospy.Rate(10)
    self.markerarray = MarkerArray()

    while not rospy.is_shutdown():

	r.sleep()

  def get_pose(self,variable):
    marker = Marker()
    self.markerarray.markers = []
    for i in range(0, self.frame_number):
      marker.pose.position.x = variable.poses[i].position.x
      marker.pose.position.y = variable.poses[i].position.y
      marker.pose.position.z = variable.poses[i].position.z
      marker.pose.orientation.x = variable.poses[i].orientation.x
      marker.pose.orientation.y = variable.poses[i].orientation.y
      marker.pose.orientation.z = variable.poses[i].orientation.z
      marker.pose.orientation.w = variable.poses[i].orientation.w
      marker.header.frame_id = "/world"
      marker.header.stamp = rospy.Time.now()
      marker.ns = "arm"+str(i)
      marker.id = 0
      marker.action = marker.ADD
      marker.type = marker.CUBE
      marker.scale.x = self.lee;
      marker.scale.y = 0.005;
      marker.scale.z = 0.005;
      marker.color.a = 1.0
      marker.color.r = 1.0
      marker.color.g = 0.0
      marker.color.b = 0.0
      marker.lifetime = rospy.Duration()
      self.markerarray.markers.append(marker)
      self.marker_pub.publish(self.markerarray)

if __name__ == '__main__':
    try:
        markers()
    except rospy.ROSInterruptException: pass
