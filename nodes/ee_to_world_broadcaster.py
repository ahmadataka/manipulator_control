#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy

import tf
from geometry_msgs.msg import Pose

if __name__ == '__main__':
    rospy.init_node('ee_to_world_broadcaster')
    rate = rospy.Rate(10.0)
    flag_listener = 0
    listener = tf.TransformListener()
    joint_num = rospy.get_param('/joint_num')
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('world', '/agent/ee'+str(joint_num-1), rospy.Time(0))
            flag_listener = 1
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        if(flag_listener==1):
            br = tf.TransformBroadcaster()
            br.sendTransform((trans[0], trans[1], trans[2]),
                             (0, 0, 0, 1),
                             rospy.Time.now(),
                             'world/ee'+str(joint_num-1),
                             "world")

        rate.sleep()
    # rospy.spin()
