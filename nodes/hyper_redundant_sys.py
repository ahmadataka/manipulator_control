#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Vector3
from sympy import *
import numpy as np

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

class system_model(object):
  def __init__(self):
    rospy.init_node('hyper_redundant_sys')
    self.joint_num = rospy.get_param('/joint_num')
    pub_pose = rospy.Publisher('/agent/joint', JointState, queue_size = 10)
    # sub_input = rospy.Subscriber('/agent/command_velocity', Float32MultiArray, self.theta_dd_input)
    sub_force = rospy.Subscriber('force', Vector3, self.force_input)
    sub_field = rospy.Subscriber('/agent/collision_avoidance', Float64MultiArray, self.get_avoidance)
    # sub_hum_force = rospy.Subscriber('human_force', Vector3, self.hum_force_input)
    # sub_contact = rospy.Subscriber('contact/point', Vector3, self.contact_input)

    self.lcm = 0.25
    self.lee = 0.5
    # theta_dd = Matrix([[0.0], [0.0]])
    # self.theta_d = theta_d_save = Matrix([[2.0], [3.0]])
    # self.theta = theta_save = Matrix([[0.0], [0.0]])
    # self.theta = [0.0, 0.0]
    self.theta = zeros(self.joint_num, 1)
    for i in range(0,self.joint_num):
        if(i==0):
            self.theta[i,0] = math.pi/2.0
        elif(i>0 and i<(self.joint_num/2)):
            self.theta[i,0] = - math.pi/6.0
        else:
            self.theta[i,0] = math.pi/6.0
    # self.torque = zeros(2,1)
    self.torque = zeros(self.joint_num, 1)
    self.system_joint = JointState()
    self.force = zeros(2,1)
    self.theta_d_avoid = zeros(self.joint_num,1)
    freq = 50.0
    self.dt = 1.0/freq
    rate = rospy.Rate(freq)
    flag = 0
    counting = 0
    self.max_norm = 0.0
    while not rospy.is_shutdown():
      joint = self.mat_to_list(self.theta)
    #   print joint
      self.inv_kin(self.jacobian_update(joint))
      self.transform_to_sent()
      pub_pose.publish(self.system_joint)
      rate.sleep()

  # def theta_dd_input(self,variable):
  #   self.torque = Matrix([[variable.data[0]], [variable.data[1]], [variable.data[1]]])
  #
  def force_input(self,variable):
    self.force = Matrix([[variable.x], [variable.y]])

  def get_avoidance(self,variable):
    for i in range(0,self.joint_num):
        self.theta_d_avoid[i,0] = variable.data[i]

  def mat_to_list(self, joint_mat):
      joint_ang = []
      for i in range(0,len(joint_mat)):
          joint_ang.append(joint_mat[i,0])
      return joint_ang

  def transform_mat(self, angle):
      T = Matrix([[cos(angle), -sin(angle), 0.0, self.lee*cos(angle)],
                  [sin(angle), cos(angle), 0.0, self.lee*sin(angle)],
                  [0.0, 0.0, 1.0, 0.0],
                  [0.0, 0.0, 0.0, 1.0]])
      return T

  def forward(self, angles):
    T_tot = eye(4)
    for i in range(0,len(angles)):
        transform = self.transform_mat(angles[i])
        T_tot = T_tot*transform
    return T_tot

  def jacobian_update(self, angles):
    T_final = self.forward(angles)
    # print T_final
    delta = 0.00001
    number = len(angles)
    angles_new = []
    Jacobi = zeros(2,number)
    for i in range(0,number):
        angles_new.append(angles[i])

    for i in range(0,number):
        angles_new[i] = angles_new[i]+delta
        T_final_new = self.forward(angles_new)
        Jacobi[0,i] = (T_final_new[0,3]-T_final[0,3])/delta
        Jacobi[1,i] = (T_final_new[1,3]-T_final[1,3])/delta
        angles_new[i] = angles_new[i]-delta
    return Jacobi

  def forward_kinematics(self, angles):
      T_out = self.forward(angles)
      pose = Matrix([[T_out[0,3]],
                     [T_out[1,3]]])
      return pose

  def inv_kin(self, jacob):
      self.theta_d = np.linalg.pinv(jacob)*self.force + self.theta_d_avoid
      self.max_norm = 3.0
    #   if(self.theta_d.norm() > self.max_norm):
    #       self.max_norm = self.theta_d.norm()
    #       print self.theta_d.norm()
      if(self.theta_d.norm()>self.max_norm):
          self.theta_d = (self.max_norm/self.theta_d.norm())*self.theta_d
    #   self.theta_d = 0.01*ones(self.joint_num,1)
    #   print "angle_speed"
    #   print np.linalg.pinv(jacob)*self.force
    #   print "jac"
    #   print jacob
      self.theta = self.theta + self.theta_d*self.dt

  def transform_to_sent(self):
    self.system_joint.header.stamp = rospy.Time.now()
    self.system_joint.name = []
    self.system_joint.position = []
    self.system_joint.velocity = []
    for i in range(0,self.joint_num):
        self.system_joint.name.append('Theta'+str(i))
        self.system_joint.position.append(self.theta[i,0])
        self.system_joint.velocity.append(self.theta_d[i,0])
    # self.system_joint.name.append('Theta2')
    # self.system_joint.position.append(self.theta[1,0])
    # self.system_joint.velocity.append(self.theta_d[1,0])

if __name__ == '__main__':
  try:
    system_model()
  except rospy.ROSInterruptException: pass
