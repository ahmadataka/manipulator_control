#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Joy

class external_force(object):
  def __init__(self):
    rospy.init_node('external_force')
    self.force = Vector3()
    self.stiffness = Vector3()
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    force_pub = rospy.Publisher('force', Vector3, queue_size = 10)
    stiffness_pub = rospy.Publisher('stiffness', Vector3, queue_size = 10)
    self.force.x = 0.0
    self.force.z = 0.0
    self.stiffness.x = 2.0
    self.stiffness.z = 2.0
    r = rospy.Rate(10.0)
    self.delta_f = 0.1
    self.delta_k = 0.1
    while not rospy.is_shutdown():
	force_pub.publish(self.force)
	stiffness_pub.publish(self.stiffness)
        r.sleep()
        
        #rospy.spin()

  def ps3_callback(self,msg):
    self.force.x -= msg.axes[2]*self.delta_f
    self.force.z -= msg.axes[3]*self.delta_f
    self.stiffness.x += msg.axes[4]*self.delta_k
    self.stiffness.z += msg.axes[5]*self.delta_k
    
if __name__ == '__main__':
    try:
        external_force()
    except rospy.ROSInterruptException: pass
