#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
from geometry_msgs.msg import PolygonStamped
from geometry_msgs.msg import Point32
from geometry_msgs.msg import PoseArray

class obstacle_movement(object):
  def __init__(self):
    rospy.init_node('obstacle_polygon')
    self.corner = PolygonStamped()
    self.rec_vel = rospy.Subscriber('/obs_pose', PoseArray, self.get_corner)
    total_pub = rospy.Publisher('/obs_polygon', PolygonStamped, queue_size = 10)

    freq = 50.0
    rate = rospy.Rate(freq)
    while not rospy.is_shutdown():
      total_pub.publish(self.corner)
      rate.sleep()

  def get_corner(self, msg):
    self.corner.polygon.points = []
    obs_point = len(msg.poses)
    for i in range(0,obs_point):
        corner_point = Point32()
        if(i<2):
            index_ = i
        else:
            index_ = obs_point+1-i
        corner_point.x = msg.poses[index_].position.x
        corner_point.y = msg.poses[index_].position.y
        self.corner.polygon.points.append(corner_point)
    self.corner.header.stamp = rospy.get_rostime()
    self.corner.header.frame_id = 'world'
    # print msg

if __name__ == '__main__':
    try:
        obstacle_movement()
    except rospy.ROSInterruptException: pass
