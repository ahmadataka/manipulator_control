#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Joy

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

class external_force(object):
  def __init__(self):
    rospy.init_node('human_force')
    self.force = Vector3()
    self.sub_dist = rospy.Subscriber('/contact/distance', Vector3, self.get_dist)
    force_pub = rospy.Publisher('human_force', Vector3, queue_size = 10)
    
    self.force.x = 0.0
    self.force.z = 0.0
    self.dist = Vector3()
    self.dist.x = 0.0
    self.dist.z = 0.25
    r = rospy.Rate(10.0)
    while not rospy.is_shutdown():
	self.generate_force()
	force_pub.publish(self.force)
        r.sleep()
        
        #rospy.spin()

  def generate_force(self):
    distance = find_magnitude(self.dist.x, self.dist.z)
    if(distance < 0.005):
      self.force.x = -self.dist.x/distance*0.5
      self.force.z = -self.dist.z/distance*0.5
    else:
      self.force.x = 0.0
      self.force.z = 0.0
    
  def get_dist(self,msg):
    self.dist.x = msg.x
    self.dist.y = msg.y
    self.dist.z = msg.z
    
if __name__ == '__main__':
    try:
        external_force()
    except rospy.ROSInterruptException: pass
