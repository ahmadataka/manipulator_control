#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

class system_model(object):
  def __init__(self):
    rospy.init_node('system_model')
    
    pub_pose = rospy.Publisher('/agent/theta', Pose, queue_size = 10)
    pub_twist = rospy.Publisher('/agent/theta_dot', Twist, queue_size = 10)
    sub_input = rospy.Subscriber('/agent/command_velocity', Vector3, self.theta_dd_input)
    sub_force = rospy.Subscriber('force', Vector3, self.force_input)
    sub_hum_force = rospy.Subscriber('human_force', Vector3, self.hum_force_input)
    sub_contact = rospy.Subscriber('contact/point', Vector3, self.contact_input)
    
    self.mass = 1
    self.g = 9.8
    self.lcm = 0.25
    self.lee = 0.5
    Inertia = (1.0/12.0)*self.mass*self.lcm**2
    B = self.mass*self.lcm**2+Inertia
    self.torque = 0.0
    self.friction = 0.0
    self.fr_c = 0.03
    self.system_twist = Twist()
    self.system_pose = Pose()
    self.force = Vector3()
    self.hum_force = Vector3()
    self.points = Vector3()
    theta_dd = 0.0
    self.theta_d = theta_d_save = 0.0
    self.theta = theta_save = math.pi/4
    
    freq = 10.0
    dt = 1.0/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.gravity_compensation()
      self.external_torque()
      self.friction = self.fr_c*self.theta_d
      theta_dd = (1/B)*(self.torque + self.torque_e - self.friction - self.G)
      self.theta_d = theta_d_save + theta_dd*dt
      self.theta = theta_save + self.theta_d*dt
      
      theta_d_save = self.theta_d
      theta_save = self.theta
      
      self.transform_to_sent()
      
      pub_pose.publish(self.system_pose)
      pub_twist.publish(self.system_twist)
      rate.sleep()  
      
  def theta_dd_input(self,variable):
    self.torque = variable.x
    
  def force_input(self,variable):
    self.force.x = variable.x
    self.force.z = variable.z
    
  def gravity_compensation(self):
    self.G = self.mass*self.g*self.lcm*math.cos(self.theta)
  
  def external_torque(self):
    Jacobi = [-self.lee*math.sin(self.theta), self.lee*math.cos(self.theta)]
    self.torque_e = Jacobi[0]*self.force.x + Jacobi[1]*self.force.z
    point_contact = find_magnitude(self.points.x, self.points.z)
    Jacobi = [-point_contact*math.sin(self.theta), point_contact*math.cos(self.theta)]
    self.torque_e += Jacobi[0]*self.hum_force.x + Jacobi[1]*self.hum_force.z
    
  def hum_force_input(self,variable):
    self.hum_force.x = variable.x
    self.hum_force.z = variable.z
    
  def contact_input(self,variable):
    self.points.x = variable.x
    self.points.z = variable.z
    
  def transform_to_sent(self):
    self.system_twist.linear.x = self.theta_d
    self.system_pose.position.x = self.theta
    
if __name__ == '__main__':
  try:
    system_model()
  except rospy.ROSInterruptException: pass