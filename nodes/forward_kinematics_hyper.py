#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3
from continuum_manipulator.msg import Vector3Array
from geometry_msgs.msg import PoseArray
import numpy as np
from sympy import lambdify
from pylab import array

class forward_kinematics(object):
  def __init__(self):
    rospy.init_node('forward_kinematics')
    self.joint_num = rospy.get_param('/joint_num')
    pub_eepose = rospy.Publisher('/agent/ee/pose', PoseArray, queue_size = 10)
    pub_cmpose = rospy.Publisher('/agent/cm/pose', PoseArray, queue_size = 10)
    pub_eetwist = rospy.Publisher('/agent/ee/twist', Vector3Array, queue_size = 10)
    sub_joint = rospy.Subscriber('/agent/joint', JointState, self.theta_input)


    self.lee = 0.02
    self.lcm = self.lee/2.0
    self.ee_pose_ar = PoseArray()
    self.cm_pose_ar = PoseArray()
    self.ee_twist = Vector3Array()
    self.theta_d = zeros(self.joint_num, 1)
    self.theta = zeros(self.joint_num, 1)

    freq = 50.0
    dt = 1.0/freq
    rate = rospy.Rate(freq)

    while not rospy.is_shutdown():

      self.forward_out()
    #   self.jacobian()

      pub_eepose.publish(self.ee_pose_ar)
      pub_eetwist.publish(self.ee_twist)
      pub_cmpose.publish(self.cm_pose_ar)
      rate.sleep()

  def theta_input(self,variable):
    index = len(variable.position)
    for i in range(0,index):
        self.theta[i,0] = variable.position[i]
        self.theta_d[i,0] = variable.velocity[i]
    #     self.theta = Matrix([[variable.position[0]], [variable.position[1]]])
    # self.theta_d = Matrix([[variable.velocity[0]], [variable.velocity[1]]])

  def transform_mat(self, angle, length):
      T = Matrix([[cos(angle), -sin(angle), 0.0, length*cos(angle)],
                  [sin(angle), cos(angle), 0.0, length*sin(angle)],
                  [0.0, 0.0, 1.0, 0.0],
                  [0.0, 0.0, 0.0, 1.0]])
      return T

  def mat_to_list(self, joint_mat, length):
      joint_ang = []
      for i in range(0,length):
          joint_ang.append(joint_mat[i,0])
      return joint_ang

  def len_to_list(self, link, length_final):
      len_list = []
      for i in range(0,link):
          if(i!=(link-1)):
              len_list.append(self.lee)
          else:
              len_list.append(length_final)
      return len_list

  def forward(self, angles, length_list):
    T_tot = eye(4)
    for i in range(0,len(angles)):
        transform = self.transform_mat(angles[i], length_list[i])
        T_tot = T_tot*transform
    return T_tot

  def jacobian_update(self, index):
    angles = self.mat_to_list(self.theta,(index))
    T_final = self.forward(angles, self.len_to_list((index), self.lee))
    # print T_final
    delta = 0.00001
    number = len(angles)
    angles_new = []
    Jacobi = zeros(2,number)
    for i in range(0,number):
        angles_new.append(angles[i])

    for i in range(0,number):
        angles_new[i] = angles_new[i]+delta
        T_final_new = self.forward(angles_new, self.len_to_list((index), self.lee))
        Jacobi[0,i] = (T_final_new[0,3]-T_final[0,3])/delta
        Jacobi[1,i] = (T_final_new[1,3]-T_final[1,3])/delta
        angles_new[i] = angles_new[i]-delta
    return Jacobi

  def forward_out(self):
    self.ee_pose_ar.poses = []
    self.cm_pose_ar.poses = []
    joint_rows = Matrix([[0.0]])
    self.ee_twist.stamp = rospy.Time.now()
    self.ee_twist.vector = []
    for i in range(0,len(self.theta)):

        T_out = self.forward(self.mat_to_list(self.theta,(i+1)), self.len_to_list((i+1), self.lee))
        quat = tf.transformations.quaternion_from_matrix(array(T_out))
        self.ee_pose = Pose()


        self.ee_pose.position.x = T_out[0,3]
        self.ee_pose.position.y = T_out[1,3]
        self.ee_pose.orientation.x = quat[0]
        self.ee_pose.orientation.y = quat[1]
        self.ee_pose.orientation.z = quat[2]
        self.ee_pose.orientation.w = quat[3]
        self.ee_pose_ar.poses.append(self.ee_pose)

        T_out = self.forward(self.mat_to_list(self.theta,(i+1)), self.len_to_list((i+1), self.lcm))
        quat = tf.transformations.quaternion_from_matrix(array(T_out))
        self.cm_pose = Pose()

        self.cm_pose.position.x = T_out[0,3]
        self.cm_pose.position.y = T_out[1,3]
        self.cm_pose.orientation.x = quat[0]
        self.cm_pose.orientation.y = quat[1]
        self.cm_pose.orientation.z = quat[2]
        self.cm_pose.orientation.w = quat[3]
        self.cm_pose_ar.poses.append(self.cm_pose)

        if(i==(self.joint_num-1)):
            jacob = self.jacobian_update(i+1)
        # if(i==0):
        #     joint_rows[0,0] = self.theta[0,0]
        # else:
        #     joint_rows = joint_rows.row_insert(i, self.theta_d.row(i))
        # vel = jacob*joint_rows
            vel = jacob*self.theta_d
            vel_vector = Vector3()
            vel_vector.x = vel[0,0]
            vel_vector.y = vel[1,0]
            self.ee_twist.vector.append(vel_vector)


    self.ee_pose_ar.header.stamp = rospy.get_rostime()
    self.cm_pose_ar.header.stamp = rospy.get_rostime()



if __name__ == '__main__':
  try:
    forward_kinematics()
  except rospy.ROSInterruptException: pass
