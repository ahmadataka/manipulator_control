#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Vector3
from std_msgs.msg import Float64MultiArray

class float_to_vector(object):
  def __init__(self):
    rospy.init_node('float_to_vector')

    sub_eepose = rospy.Subscriber('/closest_distance/vector', Float64MultiArray, self.get_distance)
    self.pub_flag = rospy.Publisher('/agent/sensor_to_obs', Vector3, queue_size = 10)
    self.dist_vec = Vector3()
    freq = 50.0
    rate = rospy.Rate(freq)

    while not rospy.is_shutdown():

      rate.sleep()

  def get_distance(self, msg):
    self.dist_vec.x = msg.data[0]
    self.dist_vec.y = msg.data[1]
    self.pub_flag.publish(self.dist_vec)

if __name__ == '__main__':
    try:
        float_to_vector()
    except rospy.ROSInterruptException: pass
