#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

class forward_kinematics(object):
  def __init__(self):
    rospy.init_node('forward_kinematics')
    
    pub_eepose = rospy.Publisher('/agent/ee/pose', Pose, queue_size = 10)
    pub_eetwist = rospy.Publisher('/agent/ee/twist', Twist, queue_size = 10)
    pub_cmpose = rospy.Publisher('/agent/cm/pose', Pose, queue_size = 10)
    pub_cmtwist = rospy.Publisher('/agent/cm/twist', Twist, queue_size = 10)
    sub_theta = rospy.Subscriber('/agent/theta', Pose, self.theta_input)
    sub_omega = rospy.Subscriber('/agent/theta_dot', Twist, self.theta_d_input)
    
    self.lcm = 0.25
    self.lee = 0.5
    
    self.ee_twist = Twist()
    self.ee_pose = Pose()
    self.cm_twist = Twist()
    self.cm_pose = Pose()
    self.theta_d = 0.0
    self.theta = math.pi/4
    
    freq = 10.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.forward()
      self.jacobian()
      
      pub_eepose.publish(self.ee_pose)
      pub_eetwist.publish(self.ee_twist)
      pub_cmpose.publish(self.cm_pose)
      pub_cmtwist.publish(self.cm_twist)
      rate.sleep()  
      
  def theta_input(self,variable):
    self.theta = variable.position.x
    
  def theta_d_input(self,variable):
    self.theta_d = variable.linear.x
    
  def forward(self):
    self.ee_pose.position.x = self.lee*math.cos(self.theta)
    self.ee_pose.position.y = 0.0
    self.ee_pose.position.z = self.lee*math.sin(self.theta)
    self.ee_pose.orientation.x = 0.0
    self.ee_pose.orientation.y = -math.sin(self.theta/2)
    self.ee_pose.orientation.z = 0.0
    self.ee_pose.orientation.w = math.cos(self.theta/2)
    
    self.cm_pose.position.x = self.lcm*math.cos(self.theta)
    self.cm_pose.position.y = 0.0
    self.cm_pose.position.z = self.lcm*math.sin(self.theta)
    self.cm_pose.orientation.x = 0.0
    self.cm_pose.orientation.y = -math.sin(self.theta/2)
    self.cm_pose.orientation.z = 0.0
    self.cm_pose.orientation.w = math.cos(self.theta/2)
    
  def jacobian(self):
    self.ee_twist.linear.x = -self.lee*math.sin(self.theta)*self.theta_d
    self.ee_twist.linear.y = 0.0
    self.ee_twist.linear.z = self.lee*math.cos(self.theta)*self.theta_d
    self.ee_twist.angular.x = 0.0
    self.ee_twist.angular.y = -self.theta_d
    self.ee_twist.angular.z = 0.0
    
    self.cm_twist.linear.x = -self.lcm*math.sin(self.theta)*self.theta_d
    self.cm_twist.linear.y = 0.0
    self.cm_twist.linear.z = self.lcm*math.cos(self.theta)*self.theta_d
    self.cm_twist.angular.x = 0.0
    self.cm_twist.angular.y = -self.theta_d
    self.cm_twist.angular.z = 0.0
    
if __name__ == '__main__':
  try:
    forward_kinematics()
  except rospy.ROSInterruptException: pass