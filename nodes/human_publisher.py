#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy

class human_publisher(object):
  def __init__(self):
    rospy.init_node('human_publisher')
    
    pub_pose = rospy.Publisher('/human/pose', Pose, queue_size = 10)
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    self.system_goal = Pose()
    self.system_goal.position.x = 0.25
    self.system_goal.position.y = 0.0
    self.system_goal.position.z = 0.25
    self.system_goal.orientation.x = 0.0
    self.system_goal.orientation.y = 0.0
    self.system_goal.orientation.z = 0.0
    self.system_goal.orientation.w = 1.0
    
    self.delta = 0.002
    freq = 10.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      pub_pose.publish(self.system_goal)
      rate.sleep()  
      
  def ps3_callback(self,msg):
    self.system_goal.position.x -= (msg.buttons[0]-msg.buttons[2])*self.delta
    self.system_goal.position.z -= (msg.buttons[1]-msg.buttons[3])*self.delta
    
if __name__ == '__main__':
  try:
    human_publisher()
  except rospy.ROSInterruptException: pass