#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseArray

class forward_kinematics(object):
  def __init__(self):
    rospy.init_node('forward_kinematics')
    
    pub_eepose = rospy.Publisher('/agent/ee/pose', PoseArray, queue_size = 10)
    pub_cmpose = rospy.Publisher('/agent/cm/pose', PoseArray, queue_size = 10)
    pub_eetwist = rospy.Publisher('/agent/ee/twist', Twist, queue_size = 10)
    sub_joint = rospy.Subscriber('/agent/joint', JointState, self.theta_input)
    
    self.lcm = 0.25
    self.lee = 0.5
    
    self.ee_pose_ar = PoseArray()
    self.cm_pose_ar = PoseArray()
    self.ee_twist = Twist()
    self.theta_d = Matrix([[0.0], [0.0]])
    self.theta = Matrix([[0.0], [0.0]])
    
    freq = 50.0
    dt = 1.0/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.forward()
      self.jacobian()
      
      pub_eepose.publish(self.ee_pose_ar)
      pub_eetwist.publish(self.ee_twist)
      pub_cmpose.publish(self.cm_pose_ar)
      rate.sleep()  
      
  def theta_input(self,variable):
    self.theta = Matrix([[variable.position[0]], [variable.position[1]]])
    self.theta_d = Matrix([[variable.velocity[0]], [variable.velocity[1]]])
    
  def forward(self):
    self.ee_pose_ar.poses = []
    self.cm_pose_ar.poses = []
    
    self.ee_pose = Pose()
    self.cm_pose = Pose()
    
    self.ee_pose.position.x = self.lee*math.cos(self.theta[0,0])
    self.ee_pose.position.y = 0.0
    self.ee_pose.position.z = self.lee*math.sin(self.theta[0,0])
    self.ee_pose.orientation.x = 0.0
    self.ee_pose.orientation.y = -math.sin(self.theta[0,0]/2)
    self.ee_pose.orientation.z = 0.0
    self.ee_pose.orientation.w = math.cos(self.theta[0,0]/2)
    
    self.cm_pose.position.x = self.lcm*math.cos(self.theta[0,0])
    self.cm_pose.position.y = 0.0
    self.cm_pose.position.z = self.lcm*math.sin(self.theta[0,0])
    self.cm_pose.orientation.x = 0.0
    self.cm_pose.orientation.y = -math.sin(self.theta[0,0]/2)
    self.cm_pose.orientation.z = 0.0
    self.cm_pose.orientation.w = math.cos(self.theta[0,0]/2)
    
    self.ee_pose_ar.poses.append(self.ee_pose)
    self.cm_pose_ar.poses.append(self.cm_pose)
    
    self.ee_pose = Pose()
    self.cm_pose = Pose()
    
    self.ee_pose.position.x = self.lee*math.cos(self.theta[0,0]) + self.lee*math.cos(self.theta[0,0] + self.theta[1,0])
    self.ee_pose.position.y = 0.0
    self.ee_pose.position.z = self.lee*math.sin(self.theta[0,0]) + self.lee*math.sin(self.theta[0,0] + self.theta[1,0])
    self.ee_pose.orientation.x = 0.0
    self.ee_pose.orientation.y = -math.sin((self.theta[0,0]+self.theta[1,0])/2)
    self.ee_pose.orientation.z = 0.0
    self.ee_pose.orientation.w = math.cos((self.theta[0,0]+self.theta[1,0])/2)
    
    self.cm_pose.position.x = self.lee*math.cos(self.theta[0,0]) + self.lcm*math.cos(self.theta[0,0]+self.theta[1,0])
    self.cm_pose.position.y = 0.0
    self.cm_pose.position.z = self.lee*math.sin(self.theta[0,0]) + self.lcm*math.sin(self.theta[0,0]+self.theta[1,0])
    self.cm_pose.orientation.x = 0.0
    self.cm_pose.orientation.y = -math.sin((self.theta[0,0]+self.theta[1,0])/2)
    self.cm_pose.orientation.z = 0.0
    self.cm_pose.orientation.w = math.cos((self.theta[0,0]+self.theta[1,0])/2)
    
    self.ee_pose_ar.poses.append(self.ee_pose)
    self.cm_pose_ar.poses.append(self.cm_pose)
    
    self.ee_pose_ar.header.stamp = rospy.get_rostime()
    self.cm_pose_ar.header.stamp = rospy.get_rostime()
    
    
  def jacobian(self):
    Jacobi = Matrix([[-self.lee*sin(self.theta[0,0]) - self.lee*sin(self.theta[0,0]+self.theta[1,0]), -self.lee*sin(self.theta[0,0]+self.theta[1,0])],
		     [self.lee*cos(self.theta[0,0])+self.lee*cos(self.theta[0,0]+self.theta[1,0]), self.lee*cos(self.theta[0,0]+self.theta[1,0])]])
    speed = Jacobi*self.theta_d
    self.ee_twist.linear.x = speed[0, 0]
    self.ee_twist.linear.y = 0.0
    self.ee_twist.linear.z = speed[1, 0]
    self.ee_twist.angular.x = 0.0
    self.ee_twist.angular.y = -(self.theta_d[0,0]+self.theta_d[1,0])
    self.ee_twist.angular.z = 0.0
    
    
if __name__ == '__main__':
  try:
    forward_kinematics()
  except rospy.ROSInterruptException: pass