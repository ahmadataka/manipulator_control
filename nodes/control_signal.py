#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

class control_signal(object):
  def __init__(self):
    rospy.init_node('control_signal')
    
    pub_torque = rospy.Publisher('/agent/command_velocity', Vector3, queue_size = 10)
    sub_pose = rospy.Subscriber('/agent/theta', Pose, self.theta_input)
    sub_twist = rospy.Subscriber('/agent/theta_dot', Twist, self.theta_d_input)
    sub_goal = rospy.Subscriber('/agent/theta_goal', Pose, self.thetag_input)
    sub_stiff = rospy.Subscriber('stiffness', Vector3, self.stiff_input)
    
    self.mass = 1
    self.g = 9.8
    self.lcm = 0.25
    Inertia = (1/12)*self.mass*self.lcm**2
    B = self.mass*self.lcm**2+Inertia
    self.torque = 0.0
    self.system_torque = Vector3()
    self.theta_d = 0.0
    self.theta = 0.0
    self.thetag = -math.pi/3
    self.KP = 2.0
    self.KD = 0.5
    freq = 10.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.gravity_compensation()
      self.torque = self.KP*(self.thetag - self.theta) - self.KD*self.theta_d + self.G
      
      self.transform_to_sent()
      
      pub_torque.publish(self.system_torque)
      rate.sleep()  
      
  def theta_input(self,variable):
    self.theta = variable.position.x
    
  def thetag_input(self,variable):
    self.thetag = variable.position.x
    
  def theta_d_input(self,variable):
    self.theta_d = variable.linear.x
  
  def stiff_input(self,variable):
    self.KP = variable.x
    self.KD = variable.y
  
  def gravity_compensation(self):
    self.G = self.mass*self.g*self.lcm*math.cos(self.theta)
  
  def transform_to_sent(self):
    self.system_torque.x = self.torque
    
if __name__ == '__main__':
  try:
    control_signal()
  except rospy.ROSInterruptException: pass