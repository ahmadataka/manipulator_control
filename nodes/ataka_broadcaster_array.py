#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import tf
from geometry_msgs.msg import PoseArray

class broadcaster(object):
  def __init__(self):
    rospy.init_node('turtle_tf_broadcaster')
    turtlename = rospy.get_param('~turtle')
    self.frame_number = rospy.get_param('~links')
    rospy.Subscriber('/%s/pose' % turtlename,
                     PoseArray,
                     self.handle_turtle_pose,
                     turtlename)
    rospy.spin()

  def handle_turtle_pose(self, msg, turtlename):
    br = [];
    for i in range(0,self.frame_number):
      ar = tf.TransformBroadcaster()
      br.append(ar)
      br[i].sendTransform((msg.poses[i].position.x, msg.poses[i].position.y, msg.poses[i].position.z),
		      (msg.poses[i].orientation.x, msg.poses[i].orientation.y, msg.poses[i].orientation.z, msg.poses[i].orientation.w),
		      rospy.Time.now(),
		      turtlename+str(i),
		      "world")
    
if __name__ == '__main__':
  try:
    broadcaster()
  except rospy.ROSInterruptException: pass
  