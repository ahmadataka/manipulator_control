#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import JointState

class control_signal(object):
  def __init__(self):
    rospy.init_node('control_signal')
    self.control_type = rospy.get_param('~control_type')
    pub_torque = rospy.Publisher('/agent/command_velocity', Float32MultiArray, queue_size = 10)
    sub_pose = rospy.Subscriber('/agent/joint', JointState, self.theta_input)
    sub_pose = rospy.Subscriber('/agent/ee/pose', PoseArray, self.pose_input)
    sub_goal = rospy.Subscriber('/agent/theta_goal', Float32MultiArray, self.thetag_input)
    sub_goal = rospy.Subscriber('/agent/pose_goal', Pose, self.poseg_input)
    sub_stiff = rospy.Subscriber('stiffness', Vector3, self.stiff_input)
    
    self.mass = 1
    self.g = 9.8
    self.lcm = 0.25
    self.lee = 0.5
    self.Inertia = (1.0/12.0)*self.mass*self.lee**2
    
    self.torque = 0.0
    self.system_torque = Float32MultiArray()
    self.theta_d = Matrix([[0.0], [0.0]])
    self.theta = Matrix([[0.0], [0.0]])
    self.pose = Matrix([[0.75], [0.0]])
    self.thetag = Matrix([[-math.pi/6], [math.pi/6]])
    self.poseg = Matrix([[0.75], [0.0]])
    self.KP = 2.0
    self.KD = 0.5
    freq = 50.0
    dt = 1.0/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.gravity_compensation()
      if(self.control_type == 0):
	#self.torque = self.KP*(self.thetag - self.theta) - self.KD*self.theta_d + self.G
	self.torque = self.G
      else:
	Jacobi = Matrix([[-self.lee*sin(self.theta[0,0]) - self.lee*sin(self.theta[0,0]+self.theta[1,0]), -self.lee*sin(self.theta[0,0]+self.theta[1,0])],
			[self.lee*cos(self.theta[0,0])+self.lee*cos(self.theta[0,0]+self.theta[1,0]), self.lee*cos(self.theta[0,0]+self.theta[1,0])]])
	self.torque = Jacobi.T*self.KP*(self.poseg - self.pose) - self.KD*self.theta_d + self.G
      
      self.transform_to_sent()
      
      pub_torque.publish(self.system_torque)
      rate.sleep()  
      
  def theta_input(self,variable):
    self.theta = Matrix([[variable.position[0]], [variable.position[1]]])
    self.theta_d = Matrix([[variable.velocity[0]], [variable.velocity[1]]])
  
  def pose_input(self,variable):
    self.pose = Matrix([[variable.poses[1].position.x], [variable.poses[1].position.z]])
  
  def thetag_input(self,variable):
    self.thetag = Matrix([[variable.data[0]], [variable.data[1]]])
  
  def poseg_input(self,variable):
    self.poseg = Matrix([[variable.position.x], [variable.position.z]])
    
  def stiff_input(self,variable):
    self.KP = Matrix([[variable.x, 0.0], [0.0, variable.z]])
    
    #self.KD = variable.y
  
  def gravity_compensation(self):
    self.G = Matrix([[1.5*self.mass*self.g*self.lee*cos(self.theta[0,0]) + 0.5*self.mass*self.g*self.lee*cos(self.theta[0,0]+self.theta[1,0])], [0.5*self.mass*self.g*self.lee*cos(self.theta[0,0]+self.theta[1,0])]])
  
  def transform_to_sent(self):
    self.system_torque.data = []
    self.system_torque.data.append(self.torque[0,0])
    self.system_torque.data.append(self.torque[1,0])
    
if __name__ == '__main__':
  try:
    control_signal()
  except rospy.ROSInterruptException: pass