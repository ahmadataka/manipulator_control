#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from continuum_manipulator.msg import Vector3Array
from std_msgs.msg import String
from std_msgs.msg import Float32
from std_msgs.msg import Int32
from geometry_msgs.msg import Twist

def find_magnitude(input1, input2):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
    return magnitude

def dot_product(A, B):
    return A[0,0]*B[0,0]+A[1,0]*B[1,0]+A[2,0]*B[2,0]

def angle(A,B):
    return dot_product(A,B)/(find_magnitude(A[0,0],A[1,0])*find_magnitude(B[0,0],B[1,0]))

if __name__ == '__main__':
    rospy.init_node('self_distance')
    obstacle_number = rospy.get_param('/ObstacleNumber')
    obs_name = rospy.get_param('/ObstacleName')
    joint_num = rospy.get_param('/joint_num')
    listener = tf.TransformListener()
    listener2 = tf.TransformListener()

    turtle_vel = rospy.Publisher('/agent/sensor_to_obs', Vector3, queue_size = 10)
    index_pub = rospy.Publisher('/agent/sensor_index', Int32, queue_size = 10)

    flag_listener = 0
    dist_obs = Vector3()
    cur_obs_dir = Vector3()
    dist_obs_sca = Float32()
    obstacle_index = String()
    index_sent = Int32()
    rate = rospy.Rate(50.0)
    distance = zeros(3,1)
    while not rospy.is_shutdown():

      minim_dist = 100.0
      minim_vect = zeros(3,1)
      minim_index = 0
      # Obstacle Individual Position Vector
      for j in range(0, joint_num-1):
          obstacle_pose = []
          for i in range(j+1, (obstacle_number)):
            agent_frame = 'agent/ee'+str(j)
            obs_frame = 'agent/ee'+str(i)

            try:

                (trans,rot) = listener.lookupTransform('world', obs_frame, rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            try:
                (trans2,rot2) = listener2.lookupTransform('world', agent_frame, rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue

            obstacle_pose.append(Matrix([[trans[0]-trans2[0]], [trans[1]-trans2[1]], [trans[2]-trans2[2]]]))

            flag_listener = 1
          minim = 100
          #print(flag_listener)
          # Obstacle's closest distance
          if(flag_listener == 1):
            for i in range(0, (obstacle_number)):
              a_vec = obstacle_pose[i]
              if(i==(obstacle_number-1)):
                b_vec = obstacle_pose[0]
              else:
                b_vec = obstacle_pose[i+1]
              c_vec = b_vec - a_vec
              theta1 = angle(-a_vec, c_vec)
              theta2 = angle(b_vec, c_vec)
              if(theta1<0.0):
                d_vec = a_vec
              elif(theta2<0.0):
                d_vec = b_vec
            	    #print("Case2")
              else:
                d_vec = a_vec - dot_product(a_vec, c_vec)/pow(find_magnitude(c_vec[0,0], c_vec[1,0]),2)*c_vec
            	    #print("Case3")
              if(find_magnitude(d_vec[0,0],d_vec[1,0])<minim):
                distance = d_vec
                minim = find_magnitude(d_vec[0,0],d_vec[1,0])
                cur_obs_vec = c_vec/find_magnitude(c_vec[0,0], c_vec[1,0])
              if(obstacle_number == 2):
                break
              #print(active_obstacle)
        #   print "norm"
        #   print distance.norm()
        #   print "minim"
        #   print minim_dist
        #   print agent_frame

          if(distance.norm()<minim_dist):
              minim_dist = distance.norm()
              minim_vect = distance
              minim_index = j

      dist_obs.x = minim_vect[0,0]
      dist_obs.y = minim_vect[1,0]
      dist_obs.z = minim_vect[2,0]
      index_sent.data = minim_index

      turtle_vel.publish(dist_obs)
      index_pub.publish(index_sent)
    #   turtle_sca.publish(dist_obs_sca)
      rate.sleep()
