#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import sys
from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QRadioButton, QVBoxLayout, QSlider
from PyQt5.QtWidgets import QPushButton, QGroupBox
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QSize
import json
from std_msgs.msg import Float64MultiArray
import time

class SliderPressure(QWidget):
    def __init__(self, minimum, maximum, index, parent=None):
        super(SliderPressure, self).__init__(parent=parent)
        self.maximum = maximum
        self.minimum = minimum
        press_index = "Pressure " + str(index)
        self.title_label = QLabel(self)
        myFont=QtGui.QFont()
        myFont.setBold(True)
        self.title_label.setFont(myFont)
        self.title_label.setText(press_index)

        self.label = QLabel(self)
        self.label.setText("0 bar")

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setFocusPolicy(Qt.StrongFocus)
        self.slider.setTickPosition(QSlider.TicksBothSides)
        self.slider.setTickInterval(10)
        self.slider.setSingleStep(1)
        self.slider.valueChanged.connect(self.setLabelValue)

        self.vbox = QVBoxLayout(self)
        self.vbox.addWidget(self.title_label)
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.slider)
        self.vbox.addStretch(1)

    def setLabelValue(self, value):
        self.x = self.minimum + (float(value) / (self.slider.maximum() - self.slider.minimum())) * (self.maximum - self.minimum)
        press_value = "{0:.3g}".format(self.x)+" bar"
        self.label.setText(press_value)


class HelloWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        rospy.init_node('ncnr_gui')
        self.setMinimumSize(QSize(640, 480))
        self.setWindowTitle("ARQ QMUL Pressure Controller")

        self.pub_press = rospy.Publisher('/pressure', Float64MultiArray, queue_size = 10)
        self.press_sent = Float64MultiArray()

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        self.gridLayout = QGridLayout(self)
        centralWidget.setLayout(self.gridLayout)

        self.w1 = SliderPressure(0.0, 10.0, 1)
        self.gridLayout.addWidget(self.w1)
        self.w2 = SliderPressure(0.0, 10.0, 2)
        self.gridLayout.addWidget(self.w2)
        self.w3 = SliderPressure(0.0, 10.0, 3)
        self.gridLayout.addWidget(self.w3)
        self.w4 = SliderPressure(0.0, 10.0, 4)
        self.gridLayout.addWidget(self.w4)
        self.w5 = SliderPressure(0.0, 10.0, 5)
        self.gridLayout.addWidget(self.w5)
        self.w6 = SliderPressure(0.0, 10.0, 6)
        self.gridLayout.addWidget(self.w6)
        self.w7 = SliderPressure(0.0, 10.0, 7)
        self.gridLayout.addWidget(self.w7)
        self.w8 = SliderPressure(0.0, 10.0, 8)
        self.gridLayout.addWidget(self.w8)
        self.w9 = SliderPressure(0.0, 10.0, 9)
        self.gridLayout.addWidget(self.w9)
        self.w10 = SliderPressure(0.0, 10.0, 10)
        self.gridLayout.addWidget(self.w10)


        pybutton_actuate = QPushButton('Actuate', self)
        pybutton_actuate.clicked.connect(self.clickActuate)
        pybutton_actuate.resize(100,32)
        self.gridLayout.addWidget(pybutton_actuate)

        pybutton_reset = QPushButton('Reset', self)
        pybutton_reset.clicked.connect(self.clickReset)
        pybutton_reset.resize(100,32)
        self.gridLayout.addWidget(pybutton_reset)
        # self.pub_press.publish(self.press_sent)


    def clickActuate(self):
        QMessageBox.about(self, "Pressure sent", "Pressure value has been sent.")
        # message_sent = "{0:.3g}".format(self.w1.x)+","+"{0:.3g}".format(self.w2.x)

        # message_ = '{"p1":'+'{0:.3g}'.format(self.w1.x)+','+'"p2":'+'{0:.3g}'.format(self.w2.x)+','+'"p3":'+'{0:.3g}'.format(self.w3.x)+','+'"p4":'+'{0:.3g}'.format(self.w4.x)+','+'"p5":'+'{0:.3g}'.format(self.w5.x)+'}'
        message_ = '{"p1":'+'{0:.3g}'.format(self.w1.x)+','+'"p2":'+'{0:.3g}'.format(self.w2.x)+','+'"p3":'+'{0:.3g}'.format(self.w3.x)+','+'"p4":'+'{0:.3g}'.format(self.w4.x)+','+'"p5":'+'{0:.3g}'.format(self.w5.x)+','+'"p6":'+'{0:.3g}'.format(self.w6.x)+'"p7":'+'{0:.3g}'.format(self.w7.x)+','+'"p8":'+'{0:.3g}'.format(self.w8.x)+','+'"p9":'+'{0:.3g}'.format(self.w9.x)+','+'"p10":'+'{0:.3g}'.format(self.w10.x)+'}'
        # print message_sent
        print message_
        self.press_sent.data = []
        self.press_sent.data.append(self.w1.x)
        self.press_sent.data.append(self.w2.x)
        self.press_sent.data.append(self.w3.x)
        self.press_sent.data.append(self.w4.x)
        self.press_sent.data.append(self.w5.x)
        self.press_sent.data.append(self.w6.x)
        self.press_sent.data.append(self.w7.x)
        self.press_sent.data.append(self.w8.x)
        self.press_sent.data.append(self.w9.x)
        self.press_sent.data.append(self.w10.x)

        # Send several times
        for i in range(0,10):
            self.pub_press.publish(self.press_sent)
            time.sleep(0.1)
        # self.ser.write(message_)

    def clickReset(self):
        QMessageBox.about(self, "Reset", "Pressure value has been set to zero.")
        message_ = '{"p1":0.00,"p2":0.00,"p3":0.00,"p4":0.00,"p5":0.00,"p6":0.00,"p7":0.00,"p8":0.00,"p9":0.00,"p10":0.00}'
        pressure_num = 10
        print message_
        self.press_sent.data = []
        for i in range(0,pressure_num):
            self.press_sent.data.append(0.0)

        for i in range(0,10):
            time.sleep(0.1)
            self.pub_press.publish(self.press_sent)
        # self.ser.write(message_)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = HelloWindow()
    # while not rospy.is_shutdown():
    mainWin.show()
    sys.exit( app.exec_() )
