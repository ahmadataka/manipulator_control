#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Pose

class markers(object):
  def __init__(self):
    rospy.init_node('markers')
    
    self.cm_pose = Pose()
    lcm = 0.25
    lee = 0.5
    
    marker_pub = rospy.Publisher('markers', Marker, queue_size = 10)
    config_space_sub = rospy.Subscriber('/agent/cm/pose', Pose, self.get_pose)
    
    r = rospy.Rate(10)
    self.marker = Marker()
    
    while not rospy.is_shutdown():
	self.marker.header.frame_id = "/world"
	self.marker.header.stamp = rospy.Time.now()
	self.marker.ns = "arm"
	self.marker.id = 0
	self.marker.action = self.marker.ADD
	self.marker.type = self.marker.CUBE
        self.marker.scale.x = lee;
        self.marker.scale.y = 0.01;
        self.marker.scale.z = 0.01;
	self.marker.color.a = 1.0
	self.marker.color.r = 1.0
	self.marker.color.g = 0.0
	self.marker.color.b = 0.0
	self.marker.lifetime = rospy.Duration();
	marker_pub.publish(self.marker);
	r.sleep()

  def get_pose(self,variable):
    self.marker.pose.position.x = variable.position.x
    self.marker.pose.position.y = variable.position.y
    self.marker.pose.position.z = variable.position.z
    self.marker.pose.orientation.x = variable.orientation.x
    self.marker.pose.orientation.y = variable.orientation.y
    self.marker.pose.orientation.z = variable.orientation.z
    self.marker.pose.orientation.w = variable.orientation.w
    
if __name__ == '__main__':
    try:
        markers()
    except rospy.ROSInterruptException: pass
