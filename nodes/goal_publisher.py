#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy

class goal_publisher(object):
  def __init__(self):
    rospy.init_node('goal_publisher')
    
    pub_pose = rospy.Publisher('/agent/theta_goal', Pose, queue_size = 10)
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    self.system_goal = Pose()
    self.system_goal.position.x = 0.0
    self.delta = math.pi/10
    freq = 10.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      pub_pose.publish(self.system_goal)
      rate.sleep()  
      
  def ps3_callback(self,msg):
    self.system_goal.position.x -= msg.axes[0]*self.delta
    
if __name__ == '__main__':
  try:
    goal_publisher()
  except rospy.ROSInterruptException: pass