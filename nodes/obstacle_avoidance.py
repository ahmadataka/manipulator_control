#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sympy import *
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3
from continuum_manipulator.msg import Vector3Array
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Twist
import numpy as np
from sympy import lambdify
from pylab import array

class obstacle_avoidance(object):
  def __init__(self):
    rospy.init_node('obstacle_avoidance')
    self.joint_num = rospy.get_param('/joint_num')
    self.field = rospy.get_param('/field_type')
    pub_eetwist = rospy.Publisher('/agent/collision_avoidance', Float64MultiArray, queue_size = 10)
    sub_joint = rospy.Subscriber('/agent/ee/pose', PoseArray, self.get_pose)
    sub_distobs = rospy.Subscriber('/agent/dist_to_obs', Vector3, self.get_distobs)
    sub_joint = rospy.Subscriber('/agent/joint', JointState, self.theta_input)
    sub_twist = rospy.Subscriber('/agent/ee/twist', Vector3Array, self.get_twist)

    self.lee = 0.02
    self.lcm = self.lee/2.0
    self.theta = zeros(self.joint_num, 1)
    self.theta_d = zeros(self.joint_num, 1)
    self.current_pose = [0.13, 0.07]
    self.dist_to_obs = Vector3()
    self.vel_obs = Vector3()
    self.theta_d_obs = Float64MultiArray()
    self.sp = Twist()
    freq = 50.0
    self.dt = 1.0/freq
    rate = rospy.Rate(freq)
    self.force = zeros(2,1)
    while not rospy.is_shutdown():
      self.collision_avoid()
      jacobi = self.jacobian_update(self.joint_num)
      theta_dot = np.linalg.pinv(jacobi)*self.force
      self.send_message(theta_dot)
    #   self.jacobian()

      pub_eetwist.publish(self.theta_d_obs)
      rate.sleep()

  def get_pose(self, msg):
    joint_ind = len(msg.poses)-1
    self.current_pose[0] = msg.poses[joint_ind].position.x
    self.current_pose[1] = msg.poses[joint_ind].position.y

  def get_distobs(self, var):
      self.dist_to_obs.x = var.x
      self.dist_to_obs.y = var.y

  def theta_input(self,variable):
    index = len(variable.position)
    for i in range(0,index):
        self.theta[i,0] = variable.position[i]
        self.theta_d[i,0] = variable.velocity[i]

  def get_twist(self, var):
    self.sp.linear.x = var.vector[0].x
    self.sp.linear.y = var.vector[0].y

  def transform_mat(self, angle, length):
      T = Matrix([[cos(angle), -sin(angle), 0.0, length*cos(angle)],
                  [sin(angle), cos(angle), 0.0, length*sin(angle)],
                  [0.0, 0.0, 1.0, 0.0],
                  [0.0, 0.0, 0.0, 1.0]])
      return T

  def mat_to_list(self, joint_mat, length):
      joint_ang = []
      for i in range(0,length):
          joint_ang.append(joint_mat[i,0])
      return joint_ang

  def len_to_list(self, link, length_final):
      len_list = []
      for i in range(0,link):
          if(i!=(link-1)):
              len_list.append(self.lee)
          else:
              len_list.append(length_final)
      return len_list

  def forward(self, angles, length_list):
    T_tot = eye(4)
    for i in range(0,len(angles)):
        transform = self.transform_mat(angles[i], length_list[i])
        T_tot = T_tot*transform
    return T_tot

  def jacobian_update(self, index):
    angles = self.mat_to_list(self.theta,(index))
    T_final = self.forward(angles, self.len_to_list((index), self.lee))
    # print T_final
    delta = 0.00001
    number = len(angles)
    angles_new = []
    Jacobi = zeros(2,number)
    for i in range(0,number):
        angles_new.append(angles[i])

    for i in range(0,number):
        angles_new[i] = angles_new[i]+delta
        T_final_new = self.forward(angles_new, self.len_to_list((index), self.lee))
        Jacobi[0,i] = (T_final_new[0,3]-T_final[0,3])/delta
        Jacobi[1,i] = (T_final_new[1,3]-T_final[1,3])/delta
        angles_new[i] = angles_new[i]-delta
    return Jacobi

  def find_magnitude(self, input1, input2):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
    return magnitude

  def send_message(self, vect):
      self.theta_d_obs.data = []
      for i in range(0, self.joint_num):
          self.theta_d_obs.data.append(vect[i,0])

  def collision_avoid(self):
        dist_const = 0.0
        bound = 0.05
        constant = 0.00001
        constant2 = 5.0
        distance = self.find_magnitude(self.dist_to_obs.x, self.dist_to_obs.y) - dist_const
        speed = self.find_magnitude(self.sp.linear.x, self.sp.linear.y)
    #   speed = find_magnitude(sp.linear.x, sp.linear.y)
    #   print(speed)
        if(self.field == 0):
        	if distance > bound :
        	  self.vel_obs.x = 0
        	  self.vel_obs.y = 0
        	else :
        	  #print('Obstacle Turtle %d\n', i+2)
        	  if distance !=0 :
        	    self.vel_obs.y = -constant*(1/distance - 1/bound)*(self.dist_to_obs.y)/(distance ** 3)
        	    self.vel_obs.x = -constant*(1/distance - 1/bound)*(self.dist_to_obs.x)/(distance ** 3)
        	  else :
        	    print("Colliding...\n")
        else:
            if((self.dist_to_obs.x!=0) and (self.dist_to_obs.y!=0)):
                # obs_cur = Matrix([[current_obs.x], [current_obs.y], [current_obs.z]])
                if(distance > (bound)):
                    self.vel_obs.x = 0
                    self.vel_obs.y = 0
                else:
                    agent_cur = Matrix([[self.sp.linear.x], [self.sp.linear.y], [0.0]])
                    if(speed!=0):
                        agent_cur = agent_cur/(speed)
                    dist_from_obs = Matrix([[-1*self.dist_to_obs.x],[-1*self.dist_to_obs.y],[-1*self.dist_to_obs.z]])
                    obs_cur = agent_cur - (agent_cur.dot(dist_from_obs)/dist_from_obs.norm())*(dist_from_obs/dist_from_obs.norm())
                    mag = (obs_cur[0,0]**2+obs_cur[1,0]**2+obs_cur[2,0]**2)**0.5
                    if(mag == 0):
                        obs_cur = Matrix([[0, 1, 0],[-1, 0, 0], [0, 0, 1]])*agent_cur
                    lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
                    B = lc_cross*Matrix([[self.sp.linear.x],[self.sp.linear.y],[0]])/distance**1
                    la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
                    Force = constant2*la_cross*B
                    self.vel_obs.x = self.sp.linear.x + Force[0,0]*self.dt
                    self.vel_obs.y = self.sp.linear.y + Force[1,0]*self.dt

                    # self.vel_obs.x = Force[0,0]
                    # self.vel_obs.y = Force[1,0]

        self.force = Matrix([[self.vel_obs.x],[self.vel_obs.y]])
        # print distance
        # print self.force

if __name__ == '__main__':
  try:
    obstacle_avoidance()
  except rospy.ROSInterruptException: pass
