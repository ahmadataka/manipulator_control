#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import math
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import PoseArray

class markers(object):
  def __init__(self):
    rospy.init_node('markers')
    self.frame_number = rospy.get_param('~links')
    self.cm_pose = PoseArray()

    self.marker_pub = rospy.Publisher('markers', MarkerArray, queue_size = 10)
    cm_sub = rospy.Subscriber('/agent/cm/pose', PoseArray, self.get_pose)
    ee_sub = rospy.Subscriber('/agent/ee/pose', PoseArray, self.get_ee)

    r = rospy.Rate(10)
    self.markerarray = MarkerArray()
    self.ee_msg = 0

    while not rospy.is_shutdown():

	r.sleep()

  def get_pose(self,variable):
    marker = Marker()
    self.markerarray.markers = []
    for i in range(0, self.frame_number):
      marker.pose.position.x = variable.poses[i].position.x
      marker.pose.position.y = variable.poses[i].position.y
      marker.pose.position.z = variable.poses[i].position.z
      marker.pose.orientation.x = variable.poses[i].orientation.x
      marker.pose.orientation.y = variable.poses[i].orientation.y
      marker.pose.orientation.z = variable.poses[i].orientation.z
      marker.pose.orientation.w = variable.poses[i].orientation.w
      marker.header.frame_id = "/world"
      marker.header.stamp = rospy.Time.now()
      marker.ns = "arm"+str(i)
      marker.id = 0
      marker.action = marker.ADD
      marker.type = marker.CUBE
      if(self.ee_msg == 1):
          marker.scale.x = self.lee[i];
      else:
          marker.scale.x = 0.0;

      marker.scale.y = 0.05;
      marker.scale.z = 0.05;
      marker.color.a = 1.0
      marker.color.r = 1.0
      marker.color.g = 0.0
      marker.color.b = 0.0
      marker.lifetime = rospy.Duration()
      self.markerarray.markers.append(marker)
      self.marker_pub.publish(self.markerarray)

  def get_ee(self,variable):
    if(self.ee_msg == 0):
        self.lee = []
        for i in range(0, self.frame_number):
            if(i!=0):
                self.lee.append(((variable.poses[i].position.x-variable.poses[i-1].position.x)**2 + (variable.poses[i].position.y-variable.poses[i-1].position.y)**2 + (variable.poses[i].position.z-variable.poses[i-1].position.z)**2 )**0.5)
            else:
                self.lee.append(((variable.poses[i].position.x)**2 + (variable.poses[i].position.y)**2 + (variable.poses[i].position.z)**2 )**0.5)
        print self.lee
    self.ee_msg = 1

if __name__ == '__main__':
    try:
        markers()
    except rospy.ROSInterruptException: pass
