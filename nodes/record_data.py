#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('manipulator_control')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sf_path_planning.msg import sf_msg
from sympy import *


class record_data(object):
  def __init__(self):
    rospy.init_node('record_data')
    self.obs_num = rospy.get_param('/ObstacleNumber')
    self.joint_num = rospy.get_param('/joint_num')
    send_pub = rospy.Publisher('/record_data', sf_msg, queue_size = 10)
    rec_goal = rospy.Subscriber('/goal_pose', Pose, self.goal_callback)
    rec_tip = rospy.Subscriber('/agent/ee/pose', PoseArray, self.tip_callback)
    rec_obs = rospy.Subscriber('/obs_pose', PoseArray, self.obspose_callback)
    self.send = sf_msg()
    self.obs_pose = Pose()
    self.tip_pose = Pose()

    frequency = 40.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
	self.transform_to_sent()
	send_pub.publish(self.send)
        r.sleep()

  def tip_callback(self, msg):
    self.send.tip.poses = []
    for i in range(0, self.joint_num):
      self.tip_pose = Pose()
      self.tip_pose.position.x = msg.poses[i].position.x
      self.tip_pose.position.y = msg.poses[i].position.y
      self.tip_pose.position.z = msg.poses[i].position.z
      self.tip_pose.orientation.w = msg.poses[i].orientation.w
      self.tip_pose.orientation.x = msg.poses[i].orientation.x
      self.tip_pose.orientation.y = msg.poses[i].orientation.y
      self.tip_pose.orientation.z = msg.poses[i].orientation.z
      self.send.tip.poses.append(self.tip_pose)

  def goal_callback(self, msg):
      self.send.goal.position.x = msg.position.x
      self.send.goal.position.y = msg.position.y
      self.send.goal.position.z = msg.position.z
      self.send.goal.orientation.w = msg.orientation.w
      self.send.goal.orientation.x = msg.orientation.x
      self.send.goal.orientation.y = msg.orientation.y
      self.send.goal.orientation.z = msg.orientation.z

  def obspose_callback(self, msg):
    self.send.obstacles.poses = []
    for i in range(0, self.obs_num):
      self.obs_pose = Pose()
      self.obs_pose.position.x = msg.poses[i].position.x
      self.obs_pose.position.y = msg.poses[i].position.y
      self.obs_pose.position.z = msg.poses[i].position.z
      self.obs_pose.orientation.w = msg.poses[i].orientation.w
      self.obs_pose.orientation.x = msg.poses[i].orientation.x
      self.obs_pose.orientation.y = msg.poses[i].orientation.y
      self.obs_pose.orientation.z = msg.poses[i].orientation.z
      self.send.obstacles.poses.append(self.obs_pose)
      #print(i)


  def transform_to_sent(self):
    # self.send.c_space.data = []
    # self.send.l_space.data = []
    # self.send.c_space.data.append(self.k[0])
    # self.send.c_space.data.append(self.p[0])
    # self.send.c_space.data.append(self.s[0])
    # self.send.c_space.data.append(self.k[1])
    # self.send.c_space.data.append(self.p[1])
    # self.send.c_space.data.append(self.s[1])
    # self.send.c_space.data.append(self.k[2])
    # self.send.c_space.data.append(self.p[2])
    # self.send.c_space.data.append(self.s[2])
    # self.send.l_space.data.append(self.l[0])
    # self.send.l_space.data.append(self.l[1])
    # self.send.l_space.data.append(self.l[2])
    # self.send.l_space.data.append(self.l[3])
    # self.send.l_space.data.append(self.l[4])
    # self.send.l_space.data.append(self.l[5])
    # self.send.l_space.data.append(self.l[6])
    # self.send.l_space.data.append(self.l[7])
    # self.send.l_space.data.append(self.l[8])
    # self.send.base.position.x = self.position_base[0]
    # self.send.base.position.y = self.position_base[1]
    # self.send.base.position.z = self.position_base[2]
    # self.send.base.orientation.w = 0.5*sqrt(1+cos(self.angle_base[0])*cos(self.angle_base[1])+sin(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])+cos(self.angle_base[0])*cos(self.angle_base[2])+cos(self.angle_base[1])*cos(self.angle_base[2]))
    # self.send.base.orientation.x = (cos(self.angle_base[1])*sin(self.angle_base[2])-(sin(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])-cos(self.angle_base[0])*sin(self.angle_base[2])))/(4*self.send.base.orientation.w)
    # self.send.base.orientation.y = (cos(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])+sin(self.angle_base[0])*sin(self.angle_base[2])-(-sin(self.angle_base[1])))/(4*self.send.base.orientation.w)
    # self.send.base.orientation.z = (sin(self.angle_base[0])*cos(self.angle_base[1])-(cos(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])-sin(self.angle_base[0])*cos(self.angle_base[2])))/(4*self.send.base.orientation.w)

    self.send.now = rospy.get_rostime()

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
