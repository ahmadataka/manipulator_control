#!/usr/bin/env python
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from sensor_msgs.msg import Joy
from std_msgs.msg import Int32

class obstacle_movement(object):
  def __init__(self):
    rospy.init_node('obstacle')
    self.current_pose = [0.13, 0.07]
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    sub_eepose = rospy.Subscriber('/agent/ee/pose', PoseArray, self.get_pose)
    self.pub_flag = rospy.Publisher('/collision_flag', Int32, queue_size = 10)
    pub_pose = rospy.Publisher('/obs1/pose', Pose, queue_size = 10)
    pub_pose2 = rospy.Publisher('/obs2/pose', Pose, queue_size = 10)
    pub_pose3 = rospy.Publisher('/obs3/pose', Pose, queue_size = 10)
    pub_pose4 = rospy.Publisher('/obs4/pose', Pose, queue_size = 10)
    pub_pose5 = rospy.Publisher('/obs5/pose', Pose, queue_size = 10)
    pub_pose6 = rospy.Publisher('/obs6/pose', Pose, queue_size = 10)
    pub_pose7 = rospy.Publisher('/obs7/pose', Pose, queue_size = 10)
    pub_pose8 = rospy.Publisher('/obs8/pose', Pose, queue_size = 10)
    pub_pose9 = rospy.Publisher('/obs9/pose', Pose, queue_size = 10)
    pub_pose10 = rospy.Publisher('/obs10/pose', Pose, queue_size = 10)
    total_pub = rospy.Publisher('/obs_pose', PoseArray, queue_size = 10)

    freq = 50.0
    self.system_pose = Pose()
    self.system_pose2 = Pose()
    self.system_pose3 = Pose()
    self.system_pose4 = Pose()
    self.system_pose5 = Pose()
    self.system_pose6 = Pose()
    self.system_pose7 = Pose()
    self.system_pose8 = Pose()
    self.system_pose9 = Pose()
    self.system_pose10 = Pose()
    total_pose = PoseArray()
    rate = rospy.Rate(freq)
    delta = 0.3
    self.delta_x = 0.001
    self.delta_y = 0.001
    self.system_pose.position.x = -0.015
    self.system_pose.position.y = 0.1
    self.system_pose2.position.x = -0.01
    self.system_pose2.position.y = 0.08
    self.system_pose3.position.x = -0.02
    self.system_pose3.position.y = 0.1
    self.system_pose4.position.x = -0.025
    self.system_pose4.position.y = 0.08


    while not rospy.is_shutdown():
    #   system_pose.position.x = 5.3+2*delta
    #   system_pose.position.y = 5.5-2*delta
    #   system_pose3.position.y = 5.5+3*delta
    #   system_pose3.position.x = 5.3-9*delta
    #   system_pose4.position.x = 5.3-9*delta
    #   system_pose4.position.y = 5.5+10*delta
    #   system_pose5.position.x = 5.3-5*delta
    #   system_pose5.position.y = 5.5+15*delta
    #   system_pose6.position.x = 6
    #   system_pose6.position.y = 5.5+23*delta

    #   self.system_pose.position.x = -0.01
    #   self.system_pose.position.y = 0.07
    #   self.system_pose2.position.x = -0.005
    #   self.system_pose2.position.y = 0.095
    #   self.system_pose3.position.x = -0.012
    #   self.system_pose3.position.y = 0.07
    #   self.system_pose4.position.x = -0.012
    #   self.system_pose4.position.y = 0.095

    #   system_pose5.position.x = 0.01*6
    #   system_pose5.position.y = 0.01*(5.5+23*delta)
    #   system_pose6.position.x = 0.01*(9+3*delta)
    #   system_pose6.position.y = 0.01*(9-3*delta)
      #system_pose7.position.x = 5.3+4*delta
      #system_pose7.position.y = 5.5-4*delta
      #system_pose8.position.x = 5.3+5*delta
      #system_pose8.position.y = 5.5-5*delta
      #system_pose9.position.x = 5.3+6*delta
      #system_pose9.position.y = 5.5-6*delta
      #system_pose10.position.x = 5.3+7*delta
      #system_pose10.position.y = 5.5-7*delta
      self.collision_check()
      pub_pose.publish(self.system_pose)
      pub_pose2.publish(self.system_pose2)
      pub_pose3.publish(self.system_pose3)
      pub_pose4.publish(self.system_pose4)
      pub_pose5.publish(self.system_pose5)
      pub_pose6.publish(self.system_pose6)
      total_pose.poses = []
      total_pose.poses.append(self.system_pose)
      total_pose.poses.append(self.system_pose2)
      total_pose.poses.append(self.system_pose3)
      total_pose.poses.append(self.system_pose4)
    #   print total_pose
      total_pub.publish(total_pose)
      #pub_pose7.publish(system_pose7)
      #pub_pose8.publish(system_pose8)
      #pub_pose9.publish(system_pose9)
      #pub_pose10.publish(system_pose10)
      rate.sleep()

  def ps3_callback(self,msg):
    self.system_pose.position.x += msg.axes[4]*self.delta_x
    self.system_pose.position.y += msg.axes[5]*self.delta_y
    self.system_pose2.position.x += msg.axes[4]*self.delta_x
    self.system_pose2.position.y += msg.axes[5]*self.delta_y
    self.system_pose3.position.x += msg.axes[4]*self.delta_x
    self.system_pose3.position.y += msg.axes[5]*self.delta_y
    self.system_pose4.position.x += msg.axes[4]*self.delta_x
    self.system_pose4.position.y += msg.axes[5]*self.delta_y

  def get_pose(self, msg):
    joint_ind = len(msg.poses)-1
    self.current_pose[0] = msg.poses[joint_ind].position.x
    self.current_pose[1] = msg.poses[joint_ind].position.y

  def collision_check(self):
      flag_sent = Int32()
      if(self.current_pose[0]>self.system_pose4.position.x and self.current_pose[0]<self.system_pose2.position.x and self.current_pose[1]>self.system_pose2.position.y and self.current_pose[1]<self.system_pose.position.y):
          flag_sent.data = 1
      else:
          flag_sent.data = 0
      self.pub_flag.publish(flag_sent)


if __name__ == '__main__':
    try:
        obstacle_movement()
    except rospy.ROSInterruptException: pass
