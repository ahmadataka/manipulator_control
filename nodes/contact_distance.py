#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
from sympy import *
import tf
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

def dot_product(A, B):
  return A[0,0]*B[0,0]+A[1,0]*B[1,0]+A[2,0]*B[2,0]
  
def angle(A,B):
  return dot_product(A,B)/(find_magnitude(A[0,0],A[2,0])*find_magnitude(B[0,0],B[2,0]))

class control_signal(object):
  def __init__(self):
    rospy.init_node('contact_distance')
    
    pub_dist = rospy.Publisher('/contact/distance', Vector3, queue_size = 10)
    pub_point = rospy.Publisher('/contact/point', Vector3, queue_size = 10)
    sub_ee = rospy.Subscriber('/agent/ee/pose', Pose, self.ee_input)
    sub_human = rospy.Subscriber('/human/pose', Pose, self.get_pose)
    
    self.dist = Vector3()
    self.points = Vector3()
    self.ee_pose = Matrix([[0.5], [0.0], [0.0]])
    self.hum_pose = Matrix([[0.25], [0.0], [0.25]])
    
    freq = 10.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.distance_generator()
      self.transform_to_sent()
      
      pub_dist.publish(self.dist)
      pub_point.publish(self.points)
      rate.sleep()  
      
  def ee_input(self,variable):
    self.ee_pose = Matrix([[variable.position.x], [variable.position.y], [variable.position.z]])
  
  def get_pose(self,variable):
    self.hum_pose = Matrix([[variable.position.x], [variable.position.y], [variable.position.z]])
  
  def distance_generator(self):
    a_vec = -1*self.hum_pose
    b_vec = self.ee_pose - self.hum_pose
    c_vec = self.ee_pose
    theta1 = angle(-a_vec, c_vec)
    theta2 = angle(b_vec, c_vec)	  
    if(theta1<0.0):
      self.d_vec = -1*a_vec
    elif(theta2<0.0):
      self.d_vec = -1*b_vec
    else:
      self.d_vec = -1*(a_vec - dot_product(a_vec, c_vec)/pow(find_magnitude(c_vec[0,0], c_vec[2,0]),2)*c_vec)
    self.con_point = find_magnitude(self.hum_pose[0,0], self.hum_pose[2,0])*theta1*self.ee_pose/find_magnitude(self.ee_pose[0,0], self.ee_pose[2,0])
	    
  def transform_to_sent(self):
    self.dist.x = self.d_vec[0,0]
    self.dist.y = self.d_vec[1,0]
    self.dist.z = self.d_vec[2,0]
    self.points.x = self.con_point[0,0]
    self.points.y = self.con_point[1,0]
    self.points.z = self.con_point[2,0]
    
if __name__ == '__main__':
  try:
    control_signal()
  except rospy.ROSInterruptException: pass