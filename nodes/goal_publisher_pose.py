#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy

class goal_publisher(object):
  def __init__(self):
    rospy.init_node('goal_publisher')
    
    pub_pose = rospy.Publisher('/agent/pose_goal', Pose, queue_size = 10)
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    self.system_goal = Pose()
    self.goal = [0.75, 0.0]
    self.system_goal.position.x = self.goal[0]
    self.system_goal.position.z = self.goal[1]
    self.delta = 0.05
    freq = 50.0
    dt = 1/freq
    rate = rospy.Rate(freq)
    
    while not rospy.is_shutdown():
      self.system_goal.position.x = self.goal[0]
      self.system_goal.position.z = self.goal[1]
      pub_pose.publish(self.system_goal)
      rate.sleep()  
      
  def ps3_callback(self,msg):
    self.goal[0] -= msg.axes[0]*self.delta
    self.goal[1] -= msg.axes[1]*self.delta
    
if __name__ == '__main__':
  try:
    goal_publisher()
  except rospy.ROSInterruptException: pass