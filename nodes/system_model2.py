#!/usr/bin/env python  
import roslib
roslib.load_manifest('manipulator_control')
import rospy
import math
import tf
from sensor_msgs.msg import JointState
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Vector3
from sympy import *

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

class system_model(object):
  def __init__(self):
    rospy.init_node('system_model')
    
    pub_pose = rospy.Publisher('/agent/joint', JointState, queue_size = 10)
    sub_input = rospy.Subscriber('/agent/command_velocity', Float32MultiArray, self.theta_dd_input)
    sub_force = rospy.Subscriber('force', Vector3, self.force_input)
    sub_hum_force = rospy.Subscriber('human_force', Vector3, self.hum_force_input)
    sub_contact = rospy.Subscriber('contact/point', Vector3, self.contact_input)
    
    self.mass = 1
    self.g = 9.8
    self.lcm = 0.25
    self.lee = 0.5
    self.Inertia = (1.0/12.0)*self.mass*self.lee**2
    theta_dd = Matrix([[0.0], [0.0]])
    self.theta_d = theta_d_save = Matrix([[2.0], [3.0]])
    self.theta = theta_save = Matrix([[0.0], [0.0]])
    
    self.torque = zeros(2,1)
    self.friction = zeros(2,1)
    self.fr_c = 0.3
    self.system_joint = JointState()
    self.force = zeros(2,1)
    self.hum_force = Vector3()
    self.points = Vector3()
    
    freq = 50.0
    dt = 1.0/freq
    rate = rospy.Rate(freq)
    flag = 0
    counting = 0
    while not rospy.is_shutdown():
      self.dynamic_update()
      self.gravity_compensation()
      self.external_torque()
      self.friction = self.fr_c*self.theta_d
      
      theta_dd = self.B.inv()*(self.torque + self.torque_e - self.friction - self.G - self.C*self.theta_d)
      
      self.theta_d = theta_d_save + theta_dd*dt
      self.theta = theta_save + self.theta_d*dt
      
      theta_d_save = self.theta_d
      theta_save = self.theta
      
      #if(flag == 0):
	##print(self.B)
	#print(self.theta)
	#counting += 1
	#if(counting == 5):
	  #flag = 1
      #print(self.theta)
      self.transform_to_sent()
      pub_pose.publish(self.system_joint)
      rate.sleep()  
      
  def theta_dd_input(self,variable):
    self.torque = Matrix([[variable.data[0]], [variable.data[1]]])
    
  def force_input(self,variable):
    self.force = Matrix([[variable.x], [variable.z]])
    
  def dynamic_update(self):
    self.B = Matrix([[1.5*self.mass*self.lee**2 + 2*self.Inertia + self.mass*self.lee**2*cos(self.theta[1, 0]), 0.25*self.mass*self.lee**2 + self.Inertia + 0.5*self.mass*self.lee**2*cos(self.theta[1, 0])],
		     [0.25*self.mass*self.lee**2 + self.Inertia + 0.5*self.mass*self.lee**2*cos(self.theta[1, 0]), 0.25*self.mass*self.lee**2 + self.Inertia]])
    self.C = Matrix([[-self.mass*self.lee**2*sin(self.theta[1,0])*self.theta_d[1,0], -0.5*self.mass*self.lee**2*sin(self.theta[1,0])*self.theta_d[1,0]],
		     [0.5*self.mass*self.lee**2*sin(self.theta[1,0])*self.theta_d[0,0], 0.0]])
    #print(self.B.inv())
    #print(self.C)
    #print("A")
    
  def gravity_compensation(self):
    self.G = Matrix([[1.5*self.mass*self.g*self.lee*cos(self.theta[0,0]) + 0.5*self.mass*self.g*self.lee*cos(self.theta[0,0]+self.theta[1,0])], [0.5*self.mass*self.g*self.lee*cos(self.theta[0,0]+self.theta[1,0])]])
    #print(self.G)
  
  def external_torque(self):
    Jacobi = Matrix([[-self.lee*sin(self.theta[0,0]) - self.lee*sin(self.theta[0,0]+self.theta[1,0]), -self.lee*sin(self.theta[0,0]+self.theta[1,0])],
		     [self.lee*cos(self.theta[0,0])+self.lee*cos(self.theta[0,0]+self.theta[1,0]), self.lee*cos(self.theta[0,0]+self.theta[1,0])]])
    self.torque_e = Jacobi.T*self.force 
    #point_contact = find_magnitude(self.points.x, self.points.z)
    #Jacobi = [-point_contact*math.sin(self.theta), point_contact*math.cos(self.theta)]
    #self.torque_e += Jacobi[0]*self.hum_force.x + Jacobi[1]*self.hum_force.z
    
  def hum_force_input(self,variable):
    self.hum_force.x = variable.x
    self.hum_force.z = variable.z
    
  def contact_input(self,variable):
    self.points.x = variable.x
    self.points.z = variable.z
    
  def transform_to_sent(self):
    self.system_joint.header.stamp = rospy.Time.now()
    self.system_joint.name = []
    self.system_joint.position = []
    self.system_joint.velocity = []
    self.system_joint.name.append('Theta1')
    self.system_joint.position.append(self.theta[0,0])
    self.system_joint.velocity.append(self.theta_d[0,0])
    self.system_joint.name.append('Theta2')
    self.system_joint.position.append(self.theta[1,0])
    self.system_joint.velocity.append(self.theta_d[1,0])
    
if __name__ == '__main__':
  try:
    system_model()
  except rospy.ROSInterruptException: pass